//
//  QuestionList.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 16/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class QuestionList: NSObject {
    
    
    /*MARK:- Question */
    func ListQuestionEN()-> [SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        
        var mListQ : [Question] = []
        
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "Adulto", mSelected: false, mID: "GE", mValue: ""),
                                         Answer(mAnswer: "Niño", mSelected: false,mID: "GE", mValue: ""),
                                         Answer(mAnswer: "Lactante", mSelected: false,mID: "GE", mValue: "")], mTypeContainer: .ThreeOtionVertical)
        )
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Seleccione el grupo de edad a evaluar", mTypeSection: .GRUPO_EDAD, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer : [Answer(mAnswer: "Espontanea",mSelected: false, mID: "AO", mValue: "4"),
                                          Answer(mAnswer: "Estimulo verbal",mSelected: false, mID: "AO", mValue: "3"),
                                          Answer(mAnswer: "Al dolor",mSelected: false, mID: "AO", mValue: "3"),
                                          Answer(mAnswer: "Ninguna",mSelected: false, mID: "AO", mValue: "1")],
                               mTypeContainer: .FourOptionsVertical))
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Apertura ocular", mTypeSection: .APERTURA_OCULAR, mShowSection: false))
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer : [Answer(mAnswer: "Orientada",mSelected: false,mID: "RP", mValue: "5"),
                                          Answer(mAnswer: "Confusa",mSelected: false, mID: "RP", mValue: "4"),
                                          Answer(mAnswer: "Palabras inapropiadas",mSelected: false, mID: "RP", mValue: "3"),
                                          Answer(mAnswer: "Sonidos ininteligibles",mSelected: false, mID: "RP", mValue: "2"),
                                          Answer(mAnswer: "Ninguna",mSelected: false, mID: "RP", mValue: "1")],
                               mTypeContainer: .FourOptionsVertical))
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Respuesta verbal", mTypeSection: .RESPUESTA_VERBAL, mShowSection: false))
        
        mListQ.removeAll()
               mListQ.append(Question(mQuestion: "",
                                      mAnswer : [Answer(mAnswer: "Obedece",mSelected: false, mID: "RM", mValue: "6"),
                                                 Answer(mAnswer: "Localiza el dolor",mSelected: false, mID: "RM", mValue: "5"),
                                                 Answer(mAnswer: "Se retira el dolor",mSelected: false, mID: "RM", mValue: "4"),
                                                 Answer(mAnswer: "Flexion anormal",mSelected: false, mID: "RM", mValue: "3"),
                                                 Answer(mAnswer: "Respuesta extensora",mSelected: false, mID: "RM", mValue: "2"),
                                                 Answer(mAnswer: "Ninguna",mSelected: false, mID: "RM", mValue: "1")],
                                      mTypeContainer: .SixOptionsVertical))
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Mejor respuesta motora", mTypeSection: .RESPUESTA_MOTORA, mShowSection: false))
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "RESEN", mValue: "")], mTypeContainer: .ResultOption)
               )
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Resultado", mTypeSection: .RESULTADO_EN, mShowSection: false))
        
        return mSectionList
    }
    

    /*MARK:- Question */
    
    func ListQuestionRASS()-> [SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        
        var mListQ : [Question] = []
        
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "Combativo, ancioso violento", mSelected: false, mID: "AN", mValue: "4"),
                                         Answer(mAnswer: "Muy Agitado, intenta quitarse el TET, los cateteres, sondas, etc", mSelected: false, mID:  "AN", mValue: "3" ),
                                         Answer(mAnswer: "Agitados movimientos frecuentes, lucha con el ventilador ", mSelected: false, mID: "AN", mValue: "2"),
                                         Answer(mAnswer: "Ansioso, Inquieto, pero sin conductas violentas ni movimientos excesivos", mSelected: false, mID: "AN", mValue: "1")], mTypeContainer: .FourOptionsVertical)
        )
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Agitación", mTypeSection: .AGITACION, mShowSection: true))
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "MBAN", mValue: "0")], mTypeContainer: .BannerOption)
               )
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .none, mShowSection: true))
        
        mListQ.removeAll()
        
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "Adormilado despierta con la voz, mantiene los ojos abiertos mas de 10s", mSelected: false, mID: "EM", mValue: "-1"),
                                         Answer(mAnswer: "Sedación ligera, despierta con la voz no mantiene los ojos abiertos mas de 10s", mSelected: false, mID: "EM", mValue: "-2"),
                                         Answer(mAnswer: "Sedacion moderada, se mueve y abre los ojos al llamada, no dirige la mirada", mSelected: false, mID: "EM", mValue: "-3"),
                                         Answer(mAnswer: "Sedación profunda, no responde al llamado, abre los ojos a la estimulacion profunda", mSelected: false, mID: "EM", mValue: "-4"),
                                         Answer(mAnswer: "Sedación muy profunda, no hay respuesta a la estimulacion fisica.", mSelected: false, mID: "EM", mValue: "-5")], mTypeContainer: .FourOptionsVertical)
        )
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Sedación", mTypeSection: .ESTADO_MENTAL, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "RESRASS", mValue: "")], mTypeContainer: .GenericResult)
               )
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Resultado", mTypeSection: .RESULTADO_RASS, mShowSection: true))
        return mSectionList
    }
    
    /*MARK:- Question */
    
    func ListQuestionCam()->[SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        var mListQ : [Question] = []
        
        mListQ.append(Question(
            mQuestion: "¿Existe algún cambio agudo del estado mental basal?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CM", mValue: ""),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CM", mValue: "")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "¿El paciente ha tenido alguna fluctuación del estado mental en las últimas 24hrs?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CM", mValue: ""),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CM", mValue: "")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Cambio agudo del estado mental del paciente", mTypeSection: .ESTADO_MENTAL, mShowSection: true))
        
        mListQ.removeAll()
        
        mListQ.append(Question(
            mQuestion: "Voy a deletrear una palabra, aprieta mi mano cuando escucha la letra A SAVEAHAART o CASABLANCA (se tomará como error si el paciente aprieta la mano  en otra letra que no sea A o que no lo haga al escucharla).",
            mAnswer: [Answer(mAnswer: "mayor 2 errores",mSelected: false, mID: "FA", mValue: ""),
                      Answer(mAnswer: "menor 2 errores",mSelected: false, mID: "FA", mValue: "")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Falta de atención", mTypeSection: .FALTA_ATENCION, mShowSection: false))
        
        mListQ.removeAll()
        
        mListQ.append(Question(
            mQuestion: "Evaluar RASS",
            mAnswer: [Answer(mAnswer: "RASS igual a 0",mSelected: false, mID: "NC", mValue: ""),
                      Answer(mAnswer: "RASS diferente a 0",mSelected: false, mID: "NC", mValue: "")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
    
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Nivel de conciencia alterado", mTypeSection: .NIVEL_DE_CONCIENCIA, mShowSection: false))
        
        mListQ.removeAll()

        mListQ.append(Question(
            mQuestion: "Preguntas",
            mAnswer: [Answer(mAnswer: "0 a 1 errores",mSelected: false, mID: "PDO", mValue: ""),
                      Answer(mAnswer: "Más de un error",mSelected: false, mID: "PDO", mValue: "")],
            mTypeContainer: .TwoOptionCustom
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Pensamiento desorganizado (preguntarle al paciente)", mTypeSection: .PENSAMIENTO_DESORGANIZADO, mShowSection: false))
        
        
        return mSectionList
    }
    
    /*MARK:- Question */
    
    func ListQuestionCROP()-> [SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        var mListQ : [Question] = []
        
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "Relajado, neutro", mSelected: false, mID: "EF", mValue: "0"),
                                         Answer(mAnswer: "Tenso (ceño funcido)", mSelected: false, mID: "EF", mValue: "1"),
                                         Answer(mAnswer: "Muecas (tenso + pàrpados fuertemente cerrados)", mSelected: false, mID: "EF", mValue: "2")], mTypeContainer: .ThreeOtionVertical)
        )
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Expresión facial", mTypeSection: .EXPRESION_FACIAL, mShowSection: true))
        
        mListQ.removeAll()
        
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "Ausencia de movimiento", mSelected: false, mID: "MC", mValue: "0"),
                                         Answer(mAnswer: "Protección (se toca donde le duele, busca atención por medio de movimientos)", mSelected: false, mID: "MC", mValue: "1"),
                                         Answer(mAnswer: "Agitado(Empuja el tubo no obedece ordenes)", mSelected: false, mID: "MC", mValue: "2")], mTypeContainer: .ThreeOtionVertical))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Movimientos del cuerpo", mTypeSection: .MOVIMIENTOS_DEL_CUERPO, mShowSection: true))
        
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "Relajado (No resistencia movimiento pasivo)", mSelected: false, mID: "TM", mValue: "0"),
                                         Answer(mAnswer: "Tenso (resistencia al movimiento pásivo)", mSelected: false, mID: "TM", mValue: "1"),
                                         Answer(mAnswer: "Muy tenso (fuerte resistencia)", mSelected: false, mID: "TM", mValue: "2")], mTypeContainer: .ThreeOtionVertical))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Tensión muscular", mTypeSection: .TENSION_MUSCULAR, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "Bien Adaptado", mSelected: false, mID: "AV", mValue: "0"),
                                         Answer(mAnswer: "Tose pero se adapta", mSelected: false, mID: "AV", mValue: "1"),
                                         Answer(mAnswer: "Lucha con el ventilador", mSelected: false, mID: "AV", mValue: "2")], mTypeContainer: .ThreeOtionVertical))
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Adaptacion al ventilador", mTypeSection: .ADAPTACION_VENTILADOR, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "RESCAM", mValue: "")], mTypeContainer: .ResultOption)
              )
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Resultado", mTypeSection: .RESULTADO_CROP, mShowSection: true))
        return mSectionList
    }
    /*MARK:- Question */
    
    func ListQuestionEvalu()-> [SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        var mListQ : [Question] = []
        
        mListQ.append(Question(
            mQuestion: "Si el paciente tiene un catéter para drenaje externo\n ¿Se encuentra cerca y seguro para movilizar?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "E_ONE", mValue: ""),
                      Answer(mAnswer: "No",mSelected: false, mID: "E_ONE", mValue: ""),
                      Answer(mAnswer: "N/A",mSelected: false, mID: "E_ONE", mValue: "")],
            mTypeContainer: .ThreeOptionsHorizontal
        ))
            
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .E_ONE, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "¿La PIC ha sido bien controlada sin administración de manitol o soluciones hipertónicas en 24 hrs?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "E_TWO", mValue: ""),
                      Answer(mAnswer: "No",mSelected: false, mID: "E_TWO", mValue: ""),
                      Answer(mAnswer: "N/A",mSelected: false, mID: "E_TWO", mValue: "")],
            mTypeContainer: .ThreeOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .E_TWO, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "¿No cuenta con tratamiento con vasopresores a dosis alta o antihipertensivos?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "E_THREE", mValue: ""),
                      Answer(mAnswer: "No",mSelected: false, mID: "E_THREE", mValue: ""),
                      Answer(mAnswer: "N/A",mSelected: false, mID: "E_THREE", mValue: "")],
            mTypeContainer: .ThreeOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .E_THREE, mShowSection: true))
        
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "¿CAM-ICU negativo para delirium?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "E_FOUR", mValue: ""),
                      Answer(mAnswer: "No",mSelected: false, mID: "E_FOUR", mValue: ""),
                      Answer(mAnswer: "N/A",mSelected: false, mID: "E_FOUR", mValue: "")],
            mTypeContainer: .ThreeOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .E_FOUR, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "¿El paciente tiene un exámen neurológico estable?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "E_FIVE", mValue: ""),
                      Answer(mAnswer: "No",mSelected: false, mID: "E_FIVE", mValue: ""),
                      Answer(mAnswer: "N/A",mSelected: false, mID: "E_FIVE", mValue: "")],
            mTypeContainer: .ThreeOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .E_FIVE, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "Si el paciente tuvo un EVC agudo ¿Han pasado mas de 24 h del inicio de los sintómas?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "E_SIX", mValue: ""),
                      Answer(mAnswer: "No",mSelected: false, mID: "E_SIX", mValue: ""),
                      Answer(mAnswer: "N/A",mSelected: false, mID: "E_SIX", mValue: "")],
            mTypeContainer: .ThreeOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .E_SIX, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "Si el paciente tuvo un hemorragia subaracnoidea por aneurisma ¿El aneurisma ha sido tratado?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "E_SEVEN", mValue: ""),
                      Answer(mAnswer: "No",mSelected: false, mID: "E_SEVEN", mValue: ""),
                      Answer(mAnswer: "N/A",mSelected: false, mID: "E_SEVEN", mValue: "")],
            mTypeContainer: .ThreeOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .E_SEVEN, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "Si el paciente tuvo una hemorragia intracerebral espontanea ¿El volumen de hemorragia se ha mantenido estable por 24h?",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "E_EIGHT", mValue: ""),
                      Answer(mAnswer: "No",mSelected: false, mID: "E_EIGHT", mValue: ""),
                      Answer(mAnswer: "N/A",mSelected: false, mID: "E_EIGHT", mValue: "")],
            mTypeContainer: .ThreeOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .E_EIGHT, mShowSection: true))
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "RESEVALU", mValue: "")], mTypeContainer: .GenericResult)
              )
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Resultado", mTypeSection: .RESULTADO_EVALU, mShowSection: true))
        return mSectionList
    }
    
    //MARK:- Question List
    
    func ListQustionS5Q()-> [SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        var mListQ : [Question] = []
        
        mListQ.append(Question(
            mQuestion: "1.- Abre y cierra sus ojos",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "SSQ_ONE", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "SSQ_ONE", mValue: "0")],
            mTypeContainer: .TwoOptionsHCustom
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .SSQ_ONE, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "2.- Mirame",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "SSQ_TWO", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "SSQ_TWO", mValue: "0")],
            mTypeContainer: .TwoOptionsHCustom
        ))
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .SSQ_TWO, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "3.- Abra su boca y saque la lengua",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "SSQ_THREE", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "SSQ_THREE", mValue: "0")],
            mTypeContainer: .TwoOptionsHCustom
        ))
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .SSQ_THREE, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "4.- Asiente con la cabeza o Diga si con la cabeza",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "SSQ_FOUR", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "SSQ_FOUR", mValue: "0")],
            mTypeContainer: .TwoOptionsHCustom
        ))
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .SSQ_FOUR, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "5.- Levante sus cejas hasta que yo cuente 5 segundos",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "SSQ_FIVE", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "SSQ_FIVE", mValue: "0")],
            mTypeContainer: .TwoOptionsHCustom
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "", mTypeSection: .SSQ_FIVE, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "RESEVALNEU", mValue: "")], mTypeContainer: .ResultOption)
              )
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Resultado", mTypeSection: .RESULTADO_SSQ, mShowSection: true))
        
        return mSectionList
    }
    
    func ListQuestionMRC()->[SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        var mListQ : [Question] = []
        
        
        mListQ.append(Question(
            mQuestion: "0.- La contracción no es visible ni palpable",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .ListOption
        ))
        
        mListQ.append(Question(
            mQuestion: "1.- Contracción visible y palpable sin movimiento articular",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .ListOption
        ))
        
        mListQ.append(Question(
            mQuestion: "2.- Movimiento a favor de la gravedad",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .ListOption
        ))
        
        mListQ.append(Question(
            mQuestion: "3.- Moviminto en contra de la gravedad",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .ListOption
        ))
        
        mListQ.append(Question(
            mQuestion: "4.- Movimiento con resistencia minima",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .ListOption
        ))
        
        mListQ.append(Question(
            mQuestion: "5.- Normal",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .ListOption
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Puntaje", mTypeSection: .PUNTAJE, mShowSection: true))
        
        mListQ.removeAll()
        
        mListQ.append(Question(
            mQuestion: "Abduccion de hombro",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMI", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Flexión de codo",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMI", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Extension de muñeca",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMI", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Flexión de cadera",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMI", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Extensión de rodilla",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMI", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Dorsiflexion de tobillo",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMI", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Grupo muscular: izquierda", mTypeSection: .GRUPO_IZQUIERDA, mShowSection: true))
        
        mListQ.removeAll()
        
        mListQ.append(Question(
            mQuestion: "Abduccion de hombro",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMD", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Flexión de codo",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMD", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Extension de muñeca",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMD", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Flexión de cadera",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMD", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Extensión de rodilla",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMD", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        mListQ.append(Question(
            mQuestion: "Dorsiflexion de tobillo",
            mAnswer: [Answer(mAnswer: "De 0 a 5",mSelected: false, mID: "GMD", mValue: "")],
            mTypeContainer: .InputOptions
        ))
        
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Grupo muscular: Derecha", mTypeSection: .GRUPO_DERECHA, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "RESMRC", mValue: "")], mTypeContainer: .ResultOption)
              )
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Resultado", mTypeSection: .RESULTADO_MUSCULAR, mShowSection: true))
        
        return mSectionList
    }
    
    func ListQuestionSeguridadInicio()->[SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        var mListQ : [Question] = []
        
        mListQ.append(Question(
            mQuestion: "Respuesta a la estimulacion verbal",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "IN1", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "IN1", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Ausencia de agitación, confusion o problemas para seguir ordenes sencillas",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "IN2", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "IN2", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Sin aumento de PIC (menor a 15 mmHg)",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "IN3", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "IN3", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        
        mListQ.append(Question(
            mQuestion: "Sin necesidad de aumentar la sedación",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "IN4", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "IN4", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Neurologica", mTypeSection: .I_NEUROLOGICA, mShowSection: true))
        
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "FC entre 50 y 130 lpm sin datos de inestabilidad",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARD1", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARD1", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Sin datos de isquemia miocardia",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARD2", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARD2", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Ausencia de hipotension ortostática ",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARD3", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARD3", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Sin necesidad de dosis altas de aminas ",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARD4", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARD4", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "TAS mayor a 90 y menor a 200 mmHg",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARD5", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARD5", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "TAM 65 - 110 mm Hg",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARD6", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARD6", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Hemoglobina mayor a 8 d/dL",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARD7", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARD7", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Plaquetas mayor a 20 000",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARD8", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARD8", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Cardiovascular", mTypeSection: .I_CARDIOVASCULAR, mShowSection: true))
        
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "PaO2/FiO2 mayor a 200",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESP1", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESP1", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "PaCO2 menor a 50 mm Hg",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESP2", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESP2", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "pH mayor a 7.30",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESP3", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESP3", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "SpO2 mayor a 90%",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESP4", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESP4", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "PEEP menor o igual a 10 cm H2O",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESP5", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESP5", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "FiO2 menor a 0.6%",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESP6", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESP6", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "FR menor a 35 npm",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESP7", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESP7", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Respiratorio", mTypeSection: .I_RESPIRATORIO, mShowSection: true))
        
        mListQ.removeAll()
               mListQ.append(Question(mQuestion: "",
                                      mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "RESINICIO", mValue: "")], mTypeContainer: .GenericResult)
                      )
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Resultado", mTypeSection: .I_RESULTADO, mShowSection: true))
        
        return mSectionList
    }
    
    func ListQuestionSeguridadFin()-> [SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        var mListQ : [Question] = []
        
        
        mListQ.append(Question(
            mQuestion: "Mayor a 70% max calculada",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDD1", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDD1", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Mayor a 20% de incremento en la FC durante la sesión",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDD2", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDD2", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Mayor a 20% de las TAS o TAD",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDD3", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDD3", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "FC menor que 50 o mayor que 130",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDD4", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDD4", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Datos clinicos de estres cardio-respiratorio",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDD5", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDD5", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "TAS menor que 90 mmHg o mayor a 180mHg",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDD6", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDD6", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "TAS menor que 90 mmHg o mayor a 180mHg",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDD7", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDD7", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "SpO2 menor a 90%",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDD8", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDD8", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "PIC mayor a 20mmHg",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDD9", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDD9", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Cardiovascular", mTypeSection: .F_CARDIOVASCURLAR, mShowSection: true))
        
        mListQ.removeAll()
        
        mListQ.append(Question(
            mQuestion: "Agitacion",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "NEUDD1", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "NEUDD1", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Ansiedad",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "NEUDD2", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "NEUDD2", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Neurologico", mTypeSection: .F_NEUROLOGICO, mShowSection: true))
        
        mListQ.removeAll()
        
        mListQ.append(Question(
            mQuestion: "FC mayor a 130 o mayor igual a 20% de la basar",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDDD1", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDDD1", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "FC menor a 40 o mayor a 130",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDDD2", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDDD2", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Nueva arrtimia cardiaca",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDDD3", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDDD3", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Nueva arrtimia cardiaca",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDDD4", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDDD4", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Datos de infarto/isquemia",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDDD5", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDDD5", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "TAS menor a 90 mmHg o mayor a 180 mmHg",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDDD6", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDDD6", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Administracion de un nuevo vasopresor",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDDD7", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDDD7", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "TAM menor a 65 mmHg o mayor a 110 mmHg",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "CARDDD8", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "CARDDD8", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Cardiovascular", mTypeSection: .F_CARDIOVASCULAR_2, mShowSection: true))
        
        mListQ.removeAll()
        
        mListQ.append(Question(
            mQuestion: "Incremento de la PEEP",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESPP1", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESPP1", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Cambio a movilidad asistida",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESPP3", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESPP3", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Asintonias con el ventilador",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESPP5", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESPP5", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Alteraciones de la integridad del equipo",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESPP6", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESPP6", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "FR mayor a 35",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESPP7", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESPP7", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "FR mayor o igual a 20%",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESPP8", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESPP8", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "FR menor a 5 o mayor a 40",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESPP9", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESPP9", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "SpO2 menor a 88% por mas de un minuto",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "RESPP10", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "RESPP10", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Respiratorio", mTypeSection: .F_RESPIRATORIO, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "Extubación",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "GEN1", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "GEN1", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Caida sobre las rodillas",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "GEN2", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "GEN2", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Desplazamiento del tubo",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "GEN3", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "GEN3", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Diaforesis",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "GEN4", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "GEN4", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mListQ.append(Question(
            mQuestion: "Si el paciente se vuelve violento",
            mAnswer: [Answer(mAnswer: "Si",mSelected: false, mID: "GEN5", mValue: "1"),
                      Answer(mAnswer: "NO",mSelected: false, mID: "GEN5", mValue: "0")],
            mTypeContainer: .TwoOptionsHorizontal
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Generales", mTypeSection: .F_GENERALES, mShowSection: true))
        
        mListQ.removeAll()
               mListQ.append(Question(mQuestion: "",
                                      mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "RESFIN", mValue: "")], mTypeContainer: .GenericResult)
                      )
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Resultado", mTypeSection: .F_RESULTADO, mShowSection: true))

        return mSectionList
    }
    
    func ListQuestionEMovilidad()-> [SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        var mListQ : [Question] = []
        
        mListQ.append(Question(
            mQuestion: "",
            mAnswer: [
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "0"),
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "1"),
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "2"),
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "3"),
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "4"),
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "5"),
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "6"),
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "7"),
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "8"),
            Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "9")
            ,Answer(mAnswer: "",mSelected: false, mID: "EMOV", mValue: "10")],
            mTypeContainer: .ItemElevenOptionVertical
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Clasificaciones", mTypeSection: .SEL_CLAS, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(mQuestion: "",
                               mAnswer: [Answer(mAnswer: "", mSelected: false, mID: "RESMOVILIDAD", mValue: "")], mTypeContainer: .GenericResult)
        )
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "La escala de evaluacion es: ", mTypeSection: .SEL_CLASIFICACION, mShowSection: true))
        
        return mSectionList
    }
    
    func ListQuestionFomurlas()->[SectionsQuestions]{
        var mSectionList : [SectionsQuestions] = []
        var mListQ : [Question] = []
        
        mListQ.append(Question(
            mQuestion: "TAM",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .InputOptionFormulas
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "PPC", mTypeSection: .PPC, mShowSection: true))

        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "PIC",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .InputOptionFormulas
        ))
        
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "TAM", mTypeSection: .TAM, mShowSection: true))

        
        mListQ.removeAll()
        mListQ.append(Question(
                   mQuestion: "TAS",
                   mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
                   mTypeContainer: .InputOptionFormulas
               ))
        
        
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "TAM", mTypeSection: .TAM, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "TAD",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .InputOptionFormulas
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "TAM", mTypeSection: .TAM, mShowSection: true))

        
        
        mListQ.removeAll()
        mListQ.append(Question(
                   mQuestion: "FC",
                   mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
                   mTypeContainer: .InputOptionFormulas
               ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Indice de choque", mTypeSection: .INDICE_DE_CHOQUE, mShowSection: true))

        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "TAS",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .InputOptionFormulas
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Indice de choque", mTypeSection: .INDICE_DE_CHOQUE, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
                   mQuestion: "Edad",
                   mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
                   mTypeContainer: .InputOptionFormulas
               ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Indice de choque", mTypeSection: .INDICE_DE_CHOQUE, mShowSection: true))

        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "Edad",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .InputOptionFormulas
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Indice de choque", mTypeSection: .INDICE_DE_CHOQUE, mShowSection: true))

        
        mListQ.removeAll()
        mListQ.append(Question(
                   mQuestion: "PaO",
                   mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
                   mTypeContainer: .InputOptionFormulas
               ))
        
        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "FiO2",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .InputOptionFormulas
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Indice PaO/FiO2", mTypeSection: .INDICE_PAO, mShowSection: true))
        
        mListQ.removeAll()
        mListQ.append(Question(
                   mQuestion: "FR",
                   mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
                   mTypeContainer: .InputOptionFormulas
               ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Indice de choque", mTypeSection: .INDICE_DE_CHOQUE, mShowSection: true))

        mListQ.removeAll()
        mListQ.append(Question(
            mQuestion: "VOLUMEN TIDAL (em litros)",
            mAnswer: [Answer(mAnswer: "",mSelected: false, mID: "", mValue: "")],
            mTypeContainer: .InputOptionFormulas
        ))
        
        mSectionList.append(SectionsQuestions(mListSectionQuestions: mListQ, mTitleSection: "Indice de Tobin", mTypeSection: .INDICE_DE_TOBIN, mShowSection: true))
        
        return mSectionList
    }
}
