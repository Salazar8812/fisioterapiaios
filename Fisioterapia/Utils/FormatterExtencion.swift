//
//  FormatterExtencion.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 28/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

extension String {
    //Converts String to Int
    public func toInt() -> Int? {
        if let num = NumberFormatter().number(from: self) {
            return num.intValue
        } else {
            return nil
        }
    }
    
    //Converts String to Double
    public func toDouble() -> Double? {
        if let num = NumberFormatter().number(from: self) {
            return num.doubleValue
        } else {
            return nil
        }
    }
    
    /// EZSE: Converts String to Float
    public func toFloat() -> Float? {
        if let num = NumberFormatter().number(from: self) {
            return num.floatValue
        } else {
            return nil
        }
    }
    
    //Converts String to Bool
    public func toBool() -> Bool? {
        return (self as NSString).boolValue
    }
}

extension Int
{
    func toString() -> String
    {
        var myString = String(self)
        return myString
    }
}

extension Double {
    func toString() -> String {
        return String(format: "%.2f",self)
    }
}


