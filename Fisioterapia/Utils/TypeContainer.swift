//
//  TypeContainer.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 16/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

enum TypeContainer {
    case TwoOptionsHCustom
    case TwoOptionsHorizontal
    case ThreeOptionsHorizontal
    case ThreeOtionVertical
    case FourOptionsVertical
    case InputOptions
    case ListOption
    case InputOptionFormulas
    case InputOptionFormulasMaxContainer
    case ResultOption
    case BannerOption
    case SixOptionsVertical
    case GenericResult
    case TwoOptionCustom
    case ItemElevenOptionVertical
}


enum SectionQuestion {
    
    //MARK:- EN
    case GRUPO_EDAD
    case APERTURA_OCULAR
    case RESPUESTA_VERBAL
    case RESPUESTA_MOTORA
    case RESULTADO_EN
    
    //MARK:- RASS
    
    case AGITACION
    case SEDACION
    case RESULTADO_RASS
    
    //MARK:- CAM
    
    case ESTADO_MENTAL
    case FALTA_ATENCION
    case NIVEL_DE_CONCIENCIA
    case PENSAMIENTO_DESORGANIZADO
    
    //MARK:-CROP
    
    case EXPRESION_FACIAL
    case MOVIMIENTOS_DEL_CUERPO
    case TENSION_MUSCULAR
    case ADAPTACION_VENTILADOR
    case RESULTADO_CROP

    //MARK:-EVALU
    
    case E_ONE
    case E_TWO
    case E_THREE
    case E_FOUR
    case E_FIVE
    case E_SIX
    case E_SEVEN
    case E_EIGHT
    case RESULTADO_EVALU

    //MARK:- SSQ
    
    case SSQ_ONE
    case SSQ_TWO
    case SSQ_THREE
    case SSQ_FOUR
    case SSQ_FIVE
    case RESULTADO_SSQ
    
    //MARK:- E.MUSCULAR
    
    case PUNTAJE
    case GRUPO_DERECHA
    case GRUPO_IZQUIERDA
    case RESULTADO_MUSCULAR
    
    //MARK:- SEGURIDAD_INICIO
    case I_NEUROLOGICA
    case I_CARDIOVASCULAR
    case I_RESPIRATORIO
    case I_RESULTADO
    
    //MARK:- SEGURIDAD FIN
    case F_CARDIOVASCURLAR
    case F_NEUROLOGICO
    case F_RESPIRATORIO
    case F_GENERALES
    case F_CARDIOVASCULAR_2
    case F_RESULTADO
    
    //MARK:- E_MOVILIDAD
    case SEL_CLAS
    case CLASIFICACIONES
    case SEL_CLASIFICACION
    
    //MARK:- FORMULAS
    case PPC
    case TAM
    case INDICE_DE_CHOQUE
    case INDICE_DE_TOBIN
    case INDICE_PAO
    
    case none
}
