//
//  FormulasViewController.swift
//  Fisioterapia
//
//  Created by Miguel Angel Campos Pacheco on 01/06/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class FormulasViewController: UIViewController, UITextFieldDelegate{
    @IBOutlet weak var mInputOneForm1: UITextField!
    @IBOutlet weak var mInputTwoForm1: UITextField!
    
    @IBOutlet weak var mInputOneForm2: UITextField!
    @IBOutlet weak var mInputTwoForm2: UITextField!
    
    @IBOutlet weak var mInputOneForm3: UITextField!
    @IBOutlet weak var mInputTwoForm3: UITextField!
    
    @IBOutlet weak var mInputOneForm4: UITextField!
    @IBOutlet weak var mInoutTwoForm4: UITextField!
    @IBOutlet weak var mInputThreeForm4: UITextField!
    
    @IBOutlet weak var mInputOneForm5: UITextField!
    @IBOutlet weak var mInputTwoForm5: UITextField!
    
    @IBOutlet weak var mInputOnwForm6: UITextField!
    @IBOutlet weak var mInputTwoForm6: UITextField!
    
    @IBOutlet weak var mBannerView: UIView!
    @IBOutlet weak var mResForm1Label: UILabel!
    @IBOutlet weak var mResForm2Label: UILabel!
    @IBOutlet weak var mResForm3Label: UILabel!
    @IBOutlet weak var mResForm4Label: UILabel!
    @IBOutlet weak var mResForm5Label: UILabel!
    @IBOutlet weak var mResForm6Label: UILabel!
    
    @IBOutlet weak var mResulForm1View: UIView!
    @IBOutlet weak var mResultForm2View: UIView!
    @IBOutlet weak var mResultForm3View: UIView!
    
    @IBOutlet weak var mResultForm4View: UIView!
    @IBOutlet weak var mResultForm5View: UIView!
    @IBOutlet weak var mResultForm6View: UIView!
    @IBOutlet weak var mHeightConstraintForm1: NSLayoutConstraint!
    @IBOutlet weak var mHeightConstraintForm2: NSLayoutConstraint!
    @IBOutlet weak var mHeightConstraintForm3: NSLayoutConstraint!
    @IBOutlet weak var mHeightConstraintForm4: NSLayoutConstraint!
    @IBOutlet weak var mHeightConstraintForm5: NSLayoutConstraint!
    @IBOutlet weak var mHeigthConstraintForm6: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundColor()
        addDelegates()
        hideViews(mView: mResulForm1View, isHide: true)
        hideViews(mView: mResultForm2View, isHide: true)
        hideViews(mView: mResultForm3View, isHide: true)
        hideViews(mView: mResultForm4View, isHide: true)
        hideViews(mView: mResultForm5View, isHide: true)
        hideViews(mView: mResultForm6View, isHide: true)
    }
    
    func hideViews(mView: UIView, isHide : Bool){
        mView.isHidden = isHide
    }
    
    func updateHeightConstraint(mView: UIView, mNewHeight : CGFloat, mHeightContraint:NSLayoutConstraint){
        mHeightContraint.constant = mNewHeight
        mView.layoutIfNeeded()
    }
    
    func setBackgroundColor(){
        mBannerView.backgroundColor = UIColor(netHex: Colors.color_blue)
    }
    
    func addDelegates(){
        mInputOneForm1.delegate = self
        mInputTwoForm1.delegate = self
        mInputOneForm2.delegate = self
        mInputTwoForm2.delegate = self
        mInputOneForm3.delegate = self
        mInputTwoForm3.delegate = self
        mInputOneForm4.delegate = self
        mInoutTwoForm4.delegate = self
        mInputOneForm5.delegate = self
        mInputTwoForm5.delegate = self
        mInputOnwForm6.delegate = self
        mInputTwoForm6.delegate = self
    }
    
    //MARK - UITextField Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789.").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if (string == numberFiltered) {
            return true
        }else{
            return false
        }
    }
    
    @IBAction func mCalcularForm1Button(_ sender: Any) {
        if(mInputOneForm1.text != "" && mInputTwoForm1.text != ""){
            let mVal1 = mInputOneForm1.text?.toDouble()
            let mVal2 = mInputTwoForm1.text?.toDouble()
            
            hideViews(mView: mResulForm1View, isHide: false)
            updateHeightConstraint(mView: mResulForm1View, mNewHeight: 350, mHeightContraint: mHeightConstraintForm1)
            let mRes = mVal1! - mVal2!
            
            if(mRes < 66){
                mResForm1Label.text =  mRes.toString() + " mmHg" + "\nConsulte con el médico encargado del paciente."
            }else{
                mResForm1Label.text = mRes.toString() + " mmHg" + "\nApto para moviliar."
            }
        }else{
            AlertDialog.show(title: "Aviso", body: "Ingresa todos los datos para continuar.", view: self)
        }
       
    }
    
    @IBAction func mCalcularForm2Button(_ sender: Any) {
        if(mInputOneForm2.text != "" && mInputTwoForm2.text != ""){
            let mVal1 = mInputOneForm2.text?.toDouble()
            let mVal2 = mInputTwoForm2.text?.toDouble()
            
            hideViews(mView: mResultForm2View, isHide: false)
            updateHeightConstraint(mView: mResultForm2View, mNewHeight: 350, mHeightContraint: mHeightConstraintForm2)
            
            let mRes = mVal1! - mVal2!
            let mRes2 = mRes / 3
            let mFinalRes = mRes2 + mVal2!
            
            if(mFinalRes < 65){
                mResForm2Label.text = mFinalRes.toString() + " mmHg" + "\nEl paciente se considera inestable."
            }else{
                mResForm2Label.text = mFinalRes.toString() + " mmHg"
            }
        }else{
            AlertDialog.show(title: "Aviso", body: "Ingresa todos los datos para continuar.", view: self)
        }
    }
    
    @IBAction func mCalcularForm3Button(_ sender: Any) {
        if(mInputOneForm3.text != "" && mInputTwoForm3.text != ""){
            let mVal1 = mInputOneForm3.text?.toDouble()
            let mVal2 = mInputTwoForm3.text?.toDouble()
            
            hideViews(mView: mResultForm3View, isHide: false)
            updateHeightConstraint(mView: mResultForm3View, mNewHeight: 340, mHeightContraint: mHeightConstraintForm3)
            
            let mRes = mVal1! / mVal2!
            
            if(mRes > 0.4 && mRes < 0.8){
                mResForm3Label.text = mRes.toString() + " mmHg" + "\nPaciente sin compromiso hemodinámico."
            }else{
                mResForm3Label.text = mRes.toString() + " mmHg" + "\nconsulte al médico tratante, paciente con compromiso hemodinámico."
            }
        }else{
            AlertDialog.show(title: "Aviso", body: "Ingresa todos los datos para continuar.", view: self)
        }
    }
    
    @IBAction func mCalcularForm4Button(_ sender: Any) {
        if(mInputOneForm4.text != "" && mInoutTwoForm4.text != "" && mInputThreeForm4.text != ""){
            let mVal1 = mInputOneForm4.text?.toDouble()
            let mVal2 = mInoutTwoForm4.text?.toDouble()
            let mVal3 = mInputThreeForm4.text?.toDouble()
            
            hideViews(mView: mResultForm4View, isHide: false)
            updateHeightConstraint(mView: mResultForm4View, mNewHeight: 420, mHeightContraint: mHeightConstraintForm4)
            
            let mFirstResult = 220 - mVal1!
            let mSecondResult = mFirstResult - mVal2!
            let mThirResult = mSecondResult * mVal3!
            
            let mResult = mThirResult + mVal2!
            mResForm4Label.text = mResult.toString()
        }else{
            AlertDialog.show(title: "Aviso", body: "Ingresa todos los datos para continuar.", view: self)
        }
    }
    @IBAction func mCalcularForm5Button(_ sender: Any) {
        if(mInputOneForm5.text != "" && mInputTwoForm5.text != ""){
            let mVal1 = mInputOneForm5.text?.toDouble()
            let mVal2 = mInputTwoForm5.text?.toDouble()
            
            hideViews(mView: mResultForm5View, isHide: false)
            updateHeightConstraint(mView: mResultForm5View, mNewHeight: 350, mHeightContraint: mHeightConstraintForm5)
            
            let mResult = mVal1! / mVal2!
            
            if(mResult > 200){
                mResForm5Label.text = mResult.toString() + "\nLa movilización puede ser segura."
            }else if(mResult < 200){
                mResForm5Label.text = mResult.toString() + "\nConsulte al médico encargado sobre la seguridad de movilizar ."
            }
        }else{
            AlertDialog.show(title: "Aviso", body: "Ingresa todos los datos para continuar.", view: self)
        }
        
    }
    @IBAction func mCalcularForm6Button(_ sender: Any) {
        if(mInputOnwForm6.text != "" && mInputTwoForm6.text != ""){
            let mVal1 = mInputOnwForm6.text?.toDouble()
            let mVal2 = mInputTwoForm6.text?.toDouble()
            
            hideViews(mView: mResultForm6View, isHide: false)
            updateHeightConstraint(mView: mResultForm6View, mNewHeight: 350, mHeightContraint: mHeigthConstraintForm6)
            
            let mResult = mVal1! / mVal2!
            
            if(mResult > 105){
                mResForm6Label.text = mResult.toString() + "\nEl paciente aún no esta listo para la prueba de respiración espontánea."
            }else if(mResult < 105){
                mResForm6Label.text = mResult.toString() + "\nPuede considerar realizar la prueba de respiración espontánea."
            }
        }else{
            AlertDialog.show(title: "Aviso", body: "Ingresa todos los datos para continuar.", view: self)
        }
    }
    
}
