//
//  GenericTabLayoutViewController.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 14/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class GenericTabLayoutViewController: UIViewController {
    @IBOutlet weak var mHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mHeaderBannerView: UIView!
    
    @IBOutlet weak var mListGenericTableView: UITableView!
    
    @IBOutlet weak var mTitleLabel: UILabel!
    @IBOutlet weak var mBodyLabel: UILabel!
    
    var mQuestionDataSource : QuestionsDataSource!
    
    var mTitle : String?
    var mBody : String?
    var mListQuestion : [SectionsQuestions] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundColor()
        mTitleLabel.text = mTitle
        mBodyLabel.text = mBody
        createTableView(mViewController: self)
        mQuestionDataSource.update(items: mListQuestion)
    }
    
    
    func createTableView(mViewController : UIViewController){
        mQuestionDataSource = QuestionsDataSource(tableView: self.mListGenericTableView, mViewController: mViewController)
         mListGenericTableView.dataSource = mQuestionDataSource
         mListGenericTableView.delegate = mQuestionDataSource
         mListGenericTableView.reloadData()
     }

    func setBackgroundColor(){
        mHeaderBannerView.backgroundColor = UIColor(netHex: Colors.color_blue)
    }
    
}
