//
//  Tab1ViewController.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 14/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//
import SlidingContainerViewController
import UIKit

class Tab1ViewController: UIViewController {
    @IBOutlet weak var mTabContainerView: UIView!
    var mArrayTitles : [String] = []
    var mNumberOfTabs : Int!
    var mArrayControllers : [GenericTabLayoutViewController] = []
    var mArrayControllersTAB : [TabEmuscularViewController] = []
    var mArrayControllersGaleria : [UIViewController] = []
    
    @IBOutlet weak var mMainContainerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(mArrayControllersTAB.count > 0 ){
            setDataSourceTabsController()
        }else if(mArrayControllersGaleria.count > 0){
            setDataSourceTabsGaleria()
        }else{
            setDataSourceTabs()
        }
        setBackgroundColor()
    
    }
    
    func setBackgroundColor(){
        mMainContainerView.backgroundColor = UIColor(netHex: Colors.color_blue)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setDataSourceTabsController(){
        let slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: mArrayControllersTAB,
            titles: mArrayTitles)
        if(mArrayTitles.count == 1){
            slidingContainerViewController.sliderView.appearance.selectorHeight = 0
        }
        mTabContainerView.addSubview(slidingContainerViewController.view)
    }
    
    func setDataSourceTabs(){
        let slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: mArrayControllers,
            titles: mArrayTitles)
        if(mArrayTitles.count == 1){
            slidingContainerViewController.sliderView.appearance.selectorHeight = 0
        }
        mTabContainerView.addSubview(slidingContainerViewController.view)
    
    }
    
    func setDataSourceTabsGaleria(){
        let slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: mArrayControllersGaleria,
            titles: mArrayTitles)
        if(mArrayTitles.count == 1){
            slidingContainerViewController.sliderView.appearance.selectorHeight = 0
        }
        mTabContainerView.addSubview(slidingContainerViewController.view)
    }
    
}

