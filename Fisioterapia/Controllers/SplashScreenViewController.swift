//
//  SplashScreenViewController.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 13/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {
    var counter = 0
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delayLaunchIntroductionView()
    }
    
    func launchHome(){
        let storyboard : UIStoryboard = UIStoryboard(name: "MainMenu", bundle: nil)
        let vc : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
        //let navigationController = UINavigationController(rootViewController: vc)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func delayLaunchIntroductionView(){
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(actionDelayHome), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelayHome() {
        counter += 1
        if(counter == 5){
            timer.invalidate()
            launchHome()
        }
    }
}
