//
//  MainMenuViewController.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 13/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController {
    @IBOutlet weak var mMainContainerView: UIView!
    
    @IBOutlet weak var mTab1Button: UIButton!
    @IBOutlet weak var mTab2Button: UIButton!
    @IBOutlet weak var mTab3Button: UIButton!
    @IBOutlet weak var mTab4Button: UIButton!
    @IBOutlet weak var mTab5Button: UIButton!
    @IBOutlet weak var mTitleTab1Label: UILabel!
    @IBOutlet weak var mIconTab1ImageView: UIImageView!
    
    @IBOutlet weak var mTitleTab2Label: UILabel!
    @IBOutlet weak var mIconTab2ImageView: UIImageView!
    
    @IBOutlet weak var mTitleTab3Label: UILabel!
    @IBOutlet weak var mIconTab3ImageView: UIImageView!
    
    @IBOutlet weak var mTitleTab4Label: UILabel!
    @IBOutlet weak var mIconTab4ImageView: UIImageView!
    
    @IBOutlet weak var mTitleTab5Label: UILabel!
    @IBOutlet weak var mIconTab5ImageView: UIImageView!
    
    var mMainController : Tab1ViewController!
    var counter = 0
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setSelectedTab(mImage: mIconTab1ImageView, mTitle: mTitleTab1Label, isSelected: true)
        setSelectedTab(mImage: mIconTab2ImageView, mTitle: mTitleTab2Label, isSelected: false)
        setSelectedTab(mImage: mIconTab3ImageView, mTitle: mTitleTab3Label, isSelected: false)
        setSelectedTab(mImage: mIconTab4ImageView, mTitle: mTitleTab4Label, isSelected: false)
        setSelectedTab(mImage: mIconTab5ImageView, mTitle: mTitleTab5Label, isSelected: false)
    
        delayLaunchIntroductionView()
    }
    
    func delayLaunchIntroductionView(){
           timer.invalidate()
           timer = Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(actionDelayHome), userInfo: nil, repeats: true)
       }
       
       @objc func actionDelayHome() {
               timer.invalidate()
               actionTab1()
       }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func addTargets(){
        mTab1Button.addTarget(self, action: #selector(actionTab1), for: .touchUpInside)
        mTab2Button.addTarget(self, action: #selector(actionTab2), for: .touchUpInside)
        mTab3Button.addTarget(self, action: #selector(actionTab3), for: .touchUpInside)
        mTab4Button.addTarget(self, action: #selector(actionTab4), for: .touchUpInside)
        mTab5Button.addTarget(self, action: #selector(actionTab5), for: .touchUpInside)
    }
    
    func setSelectedTab(mImage : UIImageView, mTitle: UILabel,isSelected: Bool){
        if(!isSelected){
            mTitle.textColor = UIColor(netHex: Colors.color_gray)
            mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
            mImage.tintColor = UIColor(netHex: Colors.color_gray)
        }else{
            mTitle.textColor = UIColor(netHex: Colors.color_blue)
            mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
            mImage.tintColor = UIColor(netHex: Colors.color_blue)
        }
        
    }
    
    @objc func actionTab1(){
        creaateViewController(mArrayController: createListTab1Controller(),mArrayTitles: ["EN","RASS","CAM","CPOT","EVALU","SSQ"])
        setSelectedTab(mImage: mIconTab1ImageView, mTitle: mTitleTab1Label, isSelected: true)
        setSelectedTab(mImage: mIconTab2ImageView, mTitle: mTitleTab2Label, isSelected: false)
        setSelectedTab(mImage: mIconTab3ImageView, mTitle: mTitleTab3Label, isSelected: false)
        setSelectedTab(mImage: mIconTab4ImageView, mTitle: mTitleTab4Label, isSelected: false)
        setSelectedTab(mImage: mIconTab5ImageView, mTitle: mTitleTab5Label, isSelected: false)
        
    }
    
    @objc func actionTab2(){
        createCustomThreeController(mArrayController: createListTab2Controller(),mArrayTitles: [""])
        setSelectedTab(mImage: mIconTab1ImageView, mTitle: mTitleTab1Label, isSelected: false)
        setSelectedTab(mImage: mIconTab2ImageView, mTitle: mTitleTab2Label, isSelected: true)
        setSelectedTab(mImage: mIconTab3ImageView, mTitle: mTitleTab3Label, isSelected: false)
        setSelectedTab(mImage: mIconTab4ImageView, mTitle: mTitleTab4Label, isSelected: false)
        setSelectedTab(mImage: mIconTab5ImageView, mTitle: mTitleTab5Label, isSelected: false)
    }
    
    @objc func actionTab3(){
        creaateViewController(mArrayController: createListTab3Controller(),mArrayTitles: ["SEGURIDAD INICIO", "SEGURIDAD FIN"])
        setSelectedTab(mImage: mIconTab1ImageView, mTitle: mTitleTab1Label, isSelected: false)
        setSelectedTab(mImage: mIconTab2ImageView, mTitle: mTitleTab2Label, isSelected: false)
        setSelectedTab(mImage: mIconTab3ImageView, mTitle: mTitleTab3Label, isSelected: true)
        setSelectedTab(mImage: mIconTab4ImageView, mTitle: mTitleTab4Label, isSelected: false)
        setSelectedTab(mImage: mIconTab5ImageView, mTitle: mTitleTab5Label, isSelected: false)
        
    }
    
    @objc func actionTab4(){
        creaateViewController(mArrayController: createListTab4Controller(),mArrayTitles: [""])
        setSelectedTab(mImage: mIconTab1ImageView, mTitle: mTitleTab1Label, isSelected: false)
        setSelectedTab(mImage: mIconTab2ImageView, mTitle: mTitleTab2Label, isSelected: false)
        setSelectedTab(mImage: mIconTab3ImageView, mTitle: mTitleTab3Label, isSelected: false)
        setSelectedTab(mImage: mIconTab4ImageView, mTitle: mTitleTab4Label, isSelected: true)
        setSelectedTab(mImage: mIconTab5ImageView, mTitle: mTitleTab5Label, isSelected: false)
    }
    
    @objc func actionTab5(){
        createCustomTab5(mArrayController: createListTab5Controller(),mArrayTitles: ["FÓRMULAS", "GALERÍA", "REFERENCIAS"])
        setSelectedTab(mImage: mIconTab1ImageView, mTitle: mTitleTab1Label, isSelected: false)
        setSelectedTab(mImage: mIconTab2ImageView, mTitle: mTitleTab2Label, isSelected: false)
        setSelectedTab(mImage: mIconTab3ImageView, mTitle: mTitleTab3Label, isSelected: false)
        setSelectedTab(mImage: mIconTab4ImageView, mTitle: mTitleTab4Label, isSelected: false)
        setSelectedTab(mImage: mIconTab5ImageView, mTitle: mTitleTab5Label, isSelected: true)
    }
    
    func creaateViewController(mArrayController : [GenericTabLayoutViewController], mArrayTitles: [String]){
        mMainController = (self.storyboard?.instantiateViewController(withIdentifier: "Tab1ViewController") as! Tab1ViewController)
        mMainController.mArrayTitles = mArrayTitles
        mMainController.mArrayControllers = mArrayController
        mMainContainerView.addSubview(mMainController.view)
        addChild(mMainController)
        
        mMainController.didMove(toParent: self)
    }
    
    func createCustomTab5(mArrayController : [UIViewController], mArrayTitles: [String]){
        mMainController = (self.storyboard?.instantiateViewController(withIdentifier: "Tab1ViewController") as! Tab1ViewController)
        mMainController.mArrayTitles = mArrayTitles
        mMainController.mArrayControllersGaleria = mArrayController
        mMainContainerView.addSubview(mMainController.view)
        addChild(mMainController)
        
        mMainController.didMove(toParent: self)
    }
    
    func createCustomThreeController(mArrayController : [TabEmuscularViewController], mArrayTitles: [String]){
        mMainController = (self.storyboard?.instantiateViewController(withIdentifier: "Tab1ViewController") as! Tab1ViewController)
        mMainController.mArrayTitles = mArrayTitles
        mMainController.mArrayControllersTAB = mArrayController
        mMainContainerView.addSubview(mMainController.view)
        addChild(mMainController)
        mMainController.didMove(toParent: self)
    }
    
    func createCustomThreeController(mArrayController : [GaleriaViewController], mArrayTitles: [String]){
        mMainController = (self.storyboard?.instantiateViewController(withIdentifier: "GaleriaViewController") as! Tab1ViewController)
        mMainController.mArrayTitles = mArrayTitles
        mMainController.mArrayControllersGaleria = mArrayController
        mMainContainerView.addSubview(mMainController.view)
        addChild(mMainController)
        
        mMainController.didMove(toParent: self)
    }
    
    
    func createListTab1Controller()-> [GenericTabLayoutViewController]{
        var mArrayControllers : [GenericTabLayoutViewController] = []
        
        mArrayControllers.append(createController(mTitle: "Examen Neurologico",mBody: "Consiste en la evaluación de tres ítems (apertura ocular, respuesta verbal, mejor respuesta motora) Con un puntaje que se debe dar de manera individual y total.",mListQuestions: QuestionList().ListQuestionEN()))
        
        mArrayControllers.append(createController(mTitle: "Escala de Agitación y Sedación de Richmond (RASS)", mBody: "RASS evalua el nivel de agitación o sedación, únicamente se categoriza al paciente con un valor de +4 a – 5", mListQuestions: QuestionList().ListQuestionRASS()))
        
        mArrayControllers.append(createController(mTitle: "CAM ICU para Delirium", mBody: "Se trata de una serie de preguntas con la finalidad de detectar o descartar la presencia de delirum en los pacientes críticos.", mListQuestions: QuestionList().ListQuestionCam()))
        
        mArrayControllers.append(createController(mTitle: "CPOT PARA DOLOR ", mBody: "Se trata de una evaluación para el dolor en el paciente bajo ventilación mecánica.", mListQuestions: QuestionList().ListQuestionCROP()))
        
        mArrayControllers.append(createController(mTitle: "Evaluación del paciente neurocrítoco para movilización temprana", mBody: "", mListQuestions: QuestionList().ListQuestionEvalu()))
        
        mArrayControllers.append(createController(mTitle: "Evaluación Neurológica", mBody: "Si el paciente es capaz de realizar 3 o más comandos es un paciente que coopera y está listo para iniciar movilización.", mListQuestions: QuestionList().ListQustionS5Q()))
        
        return mArrayControllers
    }
    
    func createListTab2Controller()->[TabEmuscularViewController]{
        var mArrayControllers : [TabEmuscularViewController] = []
        
        //mArrayControllers.append(createController(mTitle: "Evaluación Muscular MRC", mBody: "SE TRATA DE UNA EVALUACION QUE DA UN PUNTAJE DE 0 A 5 DE FUERZA MUSCULAR  (SERIA BUENO PONER UNA OPCION DE NO VALORABLE EN CASO DE QUE POR ALGUNA RAZON ESA EXTREMIDAD NO SE PUEDA MOVER) Y SE DEBE REALIZAR 12 VECES (COMO APARECE EN LA TABLA DE ABAJO., 6 DEL LADO IZQUIERO Y 6 DEL DERECHO) TENIENDO UN PUNTAJE MINIMO DE 0 Y UNO MÁXIMO DE 60 .", mListQuestions: QuestionList().ListQuestionMRC()))
        mArrayControllers.append(createThreeController())
        
        return mArrayControllers
    }
    
    func createListTab3Controller()->[GenericTabLayoutViewController]{
        var mArrayControllers : [GenericTabLayoutViewController] = []
        
        mArrayControllers.append(createController(mTitle: "Seguridad para movilización",mBody: "Consideraciones de seguridad para iniciar protocolo de movilización temprana en paciente crítico.",mListQuestions: QuestionList().ListQuestionSeguridadInicio()))
        
        mArrayControllers.append(createController(mTitle: "Seguridad para movilización", mBody: "Cuando termina la movilización", mListQuestions: QuestionList().ListQuestionSeguridadFin()))
        
        return mArrayControllers
    }
    
    func createListTab4Controller()->[GenericTabLayoutViewController]{
        var mArrayControllers : [GenericTabLayoutViewController] = []
        
        mArrayControllers.append(createController(mTitle: "ESCALA DE MOVILIDAD EN UCI (IMS)", mBody: "Se trata de una escala que clasifica del 0 al 10 al paciente crítico de acuerdo con el nivel de movilidad.", mListQuestions: QuestionList().ListQuestionEMovilidad()))
        
        return mArrayControllers
    }
    
    func createListTab5Controller()->[UIViewController]{
        var mArrayControllers : [UIViewController] = []
        
        mArrayControllers.append(createFormulasController())
        
        mArrayControllers.append(createGaleriaController())
        
        mArrayControllers.append(crateReferenciasController())
        return mArrayControllers
    }
    func createController(mTitle : String, mBody: String, mListQuestions: [SectionsQuestions])->GenericTabLayoutViewController{
        
        
        
        let mTab1 = (self.storyboard?.instantiateViewController(withIdentifier: "GenericTabLayoutViewController") as! GenericTabLayoutViewController)
        
        mTab1.mTitle = mTitle
        mTab1.mBody = mBody
        mTab1.mListQuestion = mListQuestions
        
        return mTab1
    }
    
    func createThreeController()-> TabEmuscularViewController{
        let mTab1 = (self.storyboard?.instantiateViewController(withIdentifier: "TabEmuscularViewController") as! TabEmuscularViewController)
        
        return mTab1
    }
    
    func createGaleriaController()-> GaleriaViewController{
        let mTab1 = (self.storyboard?.instantiateViewController(withIdentifier: "GaleriaViewController") as! GaleriaViewController)
        
        return mTab1
    }
    
    func crateReferenciasController()-> ReferenciasViewController{
        let mReferencias = (self.storyboard?.instantiateViewController(withIdentifier: "ReferenciasViewController") as! ReferenciasViewController)
        
        return mReferencias
    }
    
    
    func createFormulasController()->FormulasViewController{
        let mTab1 = (self.storyboard?.instantiateViewController(withIdentifier: "FormulasViewController") as! FormulasViewController)
        
        return mTab1
    }
    
    func generateQuestions()-> [SectionsQuestions]{
        let mListQuestions : [SectionsQuestions] = []
        
        
        
        return mListQuestions
    }
    
}
