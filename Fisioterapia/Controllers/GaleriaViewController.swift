//
//  GaleriaViewController.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 31/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class GaleriaViewController: UIViewController, OnSelectItemDelegate{
    @IBOutlet weak var mGaleriaCollectionView: UICollectionView!
    var mGaleriaDataSource : GaleriaDataSource!
    var mListGaleria : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populatePhotos()
        mGaleriaDataSource = GaleriaDataSource(mListPhotos: mListGaleria, mListPhotosCollectionView: mGaleriaCollectionView, mDelegate: self)

    }
    
    func onItemSelect(mNameImage: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Preview", bundle: nil)
        let previewVC = storyBoard.instantiateViewController(withIdentifier: "PreviewViewController") as! PreviewViewController
        previewVC.mNameImage = mNameImage
        self.present(previewVC, animated: true, completion: nil)
    }
    
    func populatePhotos(){
        mListGaleria.append("imagen1.png")
        mListGaleria.append("imagen2.png")
        mListGaleria.append("imagen3.png")
        mListGaleria.append("imagen4.png")
        mListGaleria.append("imagen5.png")
        mListGaleria.append("imagen6.png")
        mListGaleria.append("imagen7.png")
        mListGaleria.append("imagen8.png")
        mListGaleria.append("imagen9.png")
        mListGaleria.append("imagen10.png")
        mListGaleria.append("imagen11.png")
        mListGaleria.append("imagen12.png")
        mListGaleria.append("imagen13.png")
        mListGaleria.append("imagen14.png")
        mListGaleria.append("imagen15.png")
        mListGaleria.append("imagen16.png")
        mListGaleria.append("imagen17.png")
        mListGaleria.append("imagen18.png")
        mListGaleria.append("imagen19.png")
        mListGaleria.append("imagen20.png")
        mListGaleria.append("imagen21.png")
    }
    
}
