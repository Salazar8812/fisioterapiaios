//
//  ReferenciasViewController.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 16/06/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ReferenciasViewController: UIViewController {
    @IBOutlet weak var mListReferenceTableView: UITableView!
    var mReferenciasDataSource : ReferenciasDataSource!
    var mListReferencias : [ReferenciaModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateReferencias()
        createTableView(mViewController: self)
        mReferenciasDataSource.update(items: mListReferencias)
    }
    
    func createTableView(mViewController : UIViewController){
          mReferenciasDataSource = ReferenciasDataSource(tableView: self.mListReferenceTableView, mViewController: mViewController)
           mListReferenceTableView.dataSource = mReferenciasDataSource
           mListReferenceTableView.delegate = mReferenciasDataSource
           mListReferenceTableView.reloadData()
    }

    func populateReferencias(){
        mListReferencias.append(ReferenciaModel(mReferencia: "1.Cameron S, Ball I, Cepinskas G, Choong K, Doherty TJ, Ellis CG, et al. Early mobilization in the critical care unit: A review of adult and pediatric literature. J Crit Care. 2015;30(4):664–72."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "2.Denehy L, Lanphere J, Needham DM. Ten reasons why ICU patients should be mobilized early. Intensive Care Med. 2017;43(1):86–90."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "3.Mejía AAC, Martínez NGM, Nieto ORP, Martínez MÁC, Tomas ED, Martínez BP. Movilización Temprana Como Prevención Y Tratamiento Para La Debilidad Adquirida En La Unidad De Cuidados Intensivos En Pacientes En Ventilación Mecánica. Experiencia En Un Hospital De Segundo Nivel. Eur Sci Journal, ESJ. 2018;14(21):19."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "4.Parry SM, Huang M, Needham DM. Evaluating physical functioning in critical care: Considerations for clinical practice and research. Crit Care. 2017;21(1):1–10."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "5.Sommers J, Engelbert RHH, Dettling-Ihnenfeldt D, Gosselink R, Spronk PE, Nollet F, et al. Physiotherapy in the intensive care unit: An evidence-based, expert driven, practical statement and rehabilitation recommendations. Clin Rehabil. 2015;29(11):1051–63."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "6.Zang K, Chen B, Wang M, Chen D, Hui L, Guo S, et al. The effect of early mobilization in critically ill patients: A meta-analysis. Nurs Crit Care. 2019;(1):1–8."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "7.Martínez CMÁ, Jones BRA, Gómez GA. El fisioterapeuta en la Unidad de Cuidados Intensivos ¿un profesional necesario? Acta Med. 2020;18(1):104-105."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "8.Ding N, Zhang Z, Zhang C, Yao L, Yang L, Jiang B, et al. What is the optimum time for initiation of early mobilization in mechanically ventilated patients? A network meta-"))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "9.Cuello-Garcia CA, Mai SHC, Simpson R, Al-Harbi S, Choong K. Early Mobilization in Critically Ill Children: A Systematic Review. J Pediatr. 2018;203:25-33.e6.analysis. PLoS One. 2019;14(10):1–12."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "10.Olkowski BF, Shah SO. Early Mobilization in the Neuro-ICU: How Far Can We Go? Neurocrit Care. 2017;27(1):141–50."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "11.Leditschke AI, Green M, Irvine J, Bissett B, Mitchell IA. What Are the Barriers to Mobilizing Intensive Care Patients? Cardiopulm Phys Ther J. 2012;23(1):26–9."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "12.Clarissa C, Salisbury L, Rodgers S, Kean S. Early mobilisation in mechanically ventilated patients: A systematic integrative review of definitions and activities. J Intensive Care. 2019;7(1):1–19."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "13.Hodgson CL, Stiller K, Needham DM, Tipping CJ, Harrold M, Baldwin CE, et al. Expert consensus and recommendations on safety criteria for active mobilization of mechanically ventilated critically ill adults. Crit Care. 2014;18(6):1–9."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "14.Rebel A, Marzano V, Green M, Johnston K, Wang J, Neeman T, et al. Mobilisation is feasible in intensive care patients receiving vasoactive therapy: An observational study. Aust Crit Care. 2019;32(2):139–46."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "15.Lipshutz AKM, Engel H, Thornton K, Gropper MA. Early Mobilization in the Intensive Care Unit. Cardiopulm Phys Ther J. 2012;3(1):10–6."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "16.Berney S, Haines K, Skinner EH, Denehy L. Safety and Feasibility of an Exercise Prescription Approach to Rehabilitation Across the Continuum of Care for Survivors of Critical Illness. Phys Ther. 2012;92(12):1524–35."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "17.Hodgson CL, Capell E, Tipping CJ. Early Mobilization of Patients in Intensive Care: Organization, Communication and Safety Factors that Influence Translation into Clinical Practice. Crit Care. 2018;22(1)."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "18.Devlin JW, Skrobik Y, Gélinas C, Needham DM, Slooter AJC, Pandharipande PP, et al. Clinical Practice Guidelines for the Prevention and Management of Pain, Agitation/Sedation, Delirium, Immobility, and Sleep Disruption in Adult Patients in the ICU. Vol. 46, Critical Care Medicine. 2018. 825–873 p."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "19.Miranda Rocha AR, Martinez BP, Maldaner da Silva VZ, Forgiarini Junior LA. Early mobilization: Why, what for and how? Med Intensiva. 2017;41(7):429–36"))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "20.Pun BT, Balas MC, Barnes-Daly MA, Thompson JL, Aldrich JM, Barr J, et al. Caring for Critically Ill Patients with the ABCDEF Bundle: Results of the ICU Liberation Collaborative in Over 15,000 Adults. Crit Care Med. 2019;47(1):3–14."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "21.Da Conceição TMA, Gonzáles AI, De Figueiredo FCXS, Rocha Vieira DS, Bündchen DC. Safety criteria to start early mobilization in intensive care units. Systematic review. Rev Bras Ter Intensiva. 2017;29(4):509–19. "))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "22.Metkus TS, Kim BS. Bedside diagnosis in the intensive care unit: Is looking overlooked? Ann Am Thorac Soc. 2015;12(10):1447–50."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "23.Subirà C, Hernández G, Vázquez A, Rodríguez-Garciá R, González-Castro A, Garciá C, et al. Effect of pressure support vs T-piece ventilation strategies during spontaneous breathing trials on successful extubation among patients receiving mechanical ventilation: A randomized clinical trial. JAMA - J Am Med Assoc. 2019;321(22):2175–82."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "24.Saladin L, Avenue A. Invited Clinical Commentary Introduction to the Movement System as the Foundation for Physical Therapist. 12(6):858–61."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "25.Arias-Fernández P, Romero-Martin M, Gómez-Salgado J, Fernández-García D. Rehabilitation and early mobilization in the critical patient: systematic review. J Phys Ther Sci. 2018;30(9):1193–201."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "26.Gattinoni L, Busana M, Giosa L, Macrì MM, Quintel M. (February 2019). Prone Positioning in Acute Respiratory Distress Syndrome. Seminars in Respiratory and Critical Care Medicine, Volume 40 (1), Pp. 94-100"))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "27.Green, M., Marzano, V., Leditschke, I. A., Mitchell, I., & Bissett, B. (2016). Mobilization of intensive care patients: a multidisciplinary practical guide for clinicians. Journal of multidisciplinary healthcare, 9, 247–256. doi:10.2147/JMDH.S99811"))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "28.Bragança RD, Ravetti CG, Barreto L, Ataíde TBLS, Carneiro RM, Teixeira AL, et al. Use of handgrip dynamometry for diagnosis and prognosis assessment of intensive care unit acquired weakness: A prospective study. Hear Lung. 2019;48(6):532–7."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "29.Wahl WL., Zalewski C, Hemmila MR. Pneumonia in the surgical intensive care unit: Is every one preventable? Surgery. 2011 150(4), 665–672. doi:10.1016/j.surg.2011.08.023."))
        
        mListReferencias.append(ReferenciaModel(mReferencia: "30.Zhang L, Hu W, Cai Z, Liu J, Wu J, Deng Y, et al. Early mobilization of critically ill patients in the intensive care unit: A systematic review and meta-analysis. PLoS One. 2019;14(10):1–16."))

    }
}
