//
//  PreviewViewController.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 02/06/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {
    @IBOutlet weak var mImagePreview: UIImageView!
    var mNameImage : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mImagePreview.image = UIImage(named: mNameImage!)
    }

    @IBAction func mCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
