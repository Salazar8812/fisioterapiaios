//
//  TabEmuscularViewController.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 31/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class TabEmuscularViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var mAHIZQTextField: UITextField!
    @IBOutlet weak var mFCIZQTextField: UITextField!
    @IBOutlet weak var mEMIZQTextField: UITextField!
    @IBOutlet weak var mFCAIZQTextField: UITextField!
    @IBOutlet weak var mERIZQTexField: UITextField!
    @IBOutlet weak var mDTIZQTextField: UITextField!
    

    @IBOutlet weak var mAHDERTextField: UITextField!
    @IBOutlet weak var mFCDERTextField: UITextField!
    @IBOutlet weak var mEMDERTextField: UITextField!
    @IBOutlet weak var mFCADERTextField: UITextField!
    @IBOutlet weak var mERDERTextField: UITextField!
    @IBOutlet weak var mDTDERTextField: UITextField!
    
    @IBOutlet weak var mBannerHeaderView: UIView!
    @IBOutlet weak var mObservacionLAbel: UILabel!
    @IBOutlet weak var mValueResultLabel: UILabel!
    
    @IBOutlet weak var mDescriptionLabel: UILabel!
    @IBOutlet weak var mTitleLabel: UILabel!
    var mListTextField : [UITextField] = []
    var mAcum : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundColor()
        addDelegates()
        validateTexxtField()
        mTitleLabel.text = "Evaluación Muscular MRC"
        mDescriptionLabel.text = "Se trata de una evaluación que da un puntaje de 0 a 5 de fuerza en 12 grupos musculares.  La debilidad adquirida en la UCI se manifiesta de manera simétrica, en caso de no ser así descartar causa neurológica."
    }
    
    func setBackgroundColor(){
        mBannerHeaderView.backgroundColor = UIColor(netHex: Colors.color_blue)
    }
    
    func addDelegates(){
        mAHIZQTextField.delegate = self
        mFCIZQTextField.delegate = self
        mEMIZQTextField.delegate = self
        mFCAIZQTextField.delegate = self
        mERIZQTexField.delegate = self
        mDTIZQTextField.delegate = self
        
        mAHDERTextField.delegate = self
        mFCDERTextField.delegate = self
        mEMDERTextField.delegate = self
        mFCADERTextField.delegate = self
        mERDERTextField.delegate = self
        mDTDERTextField.delegate = self
    }
    
    func validateTexxtField(){
        mListTextField.append(mAHIZQTextField)
        mListTextField.append(mFCIZQTextField)
        mListTextField.append(mEMIZQTextField)
        mListTextField.append(mFCAIZQTextField)
        mListTextField.append(mERIZQTexField)
        mListTextField.append(mDTIZQTextField)
        
        mListTextField.append(mAHDERTextField)
        mListTextField.append(mFCDERTextField)
        mListTextField.append(mEMDERTextField)
        mListTextField.append(mFCADERTextField)
        mListTextField.append(mERDERTextField)
        mListTextField.append(mDTDERTextField)
    }
    
    
    //MARK - UITextField Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
       let aSet = NSCharacterSet(charactersIn:"012345").inverted
       let compSepByCharInSet = string.components(separatedBy: aSet)
       let numberFiltered = compSepByCharInSet.joined(separator: "")
            
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString

        
        if (string == numberFiltered && newString.length <= maxLength) {
            return true
        }else{
            return false
        }
    }
    

    @IBAction func mCalcularButton(_ sender: Any) {
        mAcum = 0
        for mTextField in mListTextField{
            let mVal = mTextField.text == "" ? "0" : mTextField.text
            print(mVal as Any)
            mAcum = mAcum + (mVal!.toInt())!
        }
        
        if(mAcum > 37 && mAcum < 48){
            mValueResultLabel.text = mAcum.toString()
            mObservacionLAbel.text = "DEBILIDAD ADQUIRIDA EN LA UCI"
        }else if(mAcum < 38){
            mValueResultLabel.text = mAcum.toString()
            mObservacionLAbel.text = "DEBILIDAD GRAVE"
        }else if(mAcum > 48){
            mValueResultLabel.text = mAcum.toString()
            mObservacionLAbel.text = "SIN DEBILIDAD ADQUIRIDA EN LA UCI"
        }
    
    }
}
