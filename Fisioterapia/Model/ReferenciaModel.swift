//
//  ReferenciaModel.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 16/06/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ReferenciaModel: NSObject {
    var mReferencia : String = ""
    
    init(mReferencia : String){
        self.mReferencia = mReferencia
    }
}
