//
//  QuestionsModel.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 14/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class SectionsQuestions : NSObject{
    var mTitleSection : String = ""
    var mTypeSection : SectionQuestion?
    var mListSectionQuestions : [Question] = []
    var mShowSection : Bool = true
 
    init(mListSectionQuestions: [Question], mTitleSection: String, mTypeSection: SectionQuestion, mShowSection :Bool ) {
        self.mTypeSection = mTypeSection
        self.mListSectionQuestions = mListSectionQuestions
        self.mTitleSection = mTitleSection
        self.mShowSection = mShowSection
    }
    
}

class Question : NSObject{
    var mTypeContainer : TypeContainer?
    var mQuestion : String = ""
    var mAnswer : [Answer] = []
    init(mQuestion : String, mAnswer: [Answer],mTypeContainer : TypeContainer) {
        self.mTypeContainer = mTypeContainer
        self.mQuestion = mQuestion
        self.mAnswer = mAnswer
    }
}

class Answer : NSObject{
    var mAnswer : String = ""
    var mSelected : Bool = false
    var mID : String = ""
    var mValue : String = ""
    
    init(mAnswer : String, mSelected : Bool, mID : String, mValue: String) {
        self.mAnswer = mAnswer
        self.mSelected = mSelected
        self.mID = mID
        self.mValue = mValue
    }
    
}
