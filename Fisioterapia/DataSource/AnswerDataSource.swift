//
//  AnswerDataSource.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 16/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

protocol AnswernSelectedDelegate : NSObjectProtocol{
    func OnSelectedAnswer()
}

protocol HiddenViewDelegate : NSObjectProtocol{
    func OnUnSelect(mClickFrom: String)
    func OnSelectEDAD(mEdad : String)
    func hideShowSection(mTypeSection : SectionQuestion, mShowSection: Bool)
    func refreshTablaData()
}

class AnswerDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var items : [Question] = []
    var tableView : UITableView?
    var mAnswerDelegate : AnswernSelectedDelegate?
    var mHiddenViewDelegate : HiddenViewDelegate?
    
    var mThreeOptionHorizonalCell : String = "ItemThreeOptionsHorizontalTableViewCell"
    var mFourOptionVerticalCell : String = "ItemFourVerticalOptionsTableViewCell"
    var mTwoOptionCell : String = "ItemTwoOptionTableViewCell"
    var mInputOptionsCell : String = "ItemInputOptionTableViewCell"
    var mListOptionCell : String = "ItemListOptionTableViewCell"
    var mThreeOptipnVerticalCell : String = "ItemThreeVerticalOptionsTableViewCell"
    var mInputFormulasOptionCell : String = "ItemFormulasTableViewCell"
    var mResultOptionCell : String = "ItemResultadoTableViewCell"
    var mBannerOptionCell : String = "ItemHeaderBannerTableViewCell"
    var mSixOptionsVerticalCell : String = "ItemSixOptionTableViewCell"
    var mResutlGenericCell : String = "ItemResultadoGenericTableViewCell"
    var mTwoOptionCustom : String = "ItemTwoOptionCustomTableViewCell"
    var mElevenOption : String = "ItemElevenOptionTableViewCell"
    var mSection : SectionQuestion?
    var mQuestion : Question?
    var mTagPosition : Int = 0
    var mCell : UITableViewCell?
    var mCellFourOption : ItemFourVerticalOptionsTableViewCell?
    var mCellTwoOption : ItemTwoOptionTableViewCell?
    var mCellThreeOption : ItemThreeOptionsHorizontalTableViewCell?
    var mCellInputOption : ItemInputOptionTableViewCell?
    var mCellListOption : ItemListOptionTableViewCell?
    var mCellThreeVerticalOption : ItemThreeVerticalOptionsTableViewCell?
    var mCellInputFormulasOption : ItemFormulasTableViewCell?
    var mCellResultOption : ItemResultadoTableViewCell?
    var mCellBannerOption : ItemHeaderBannerTableViewCell?
    var mCellSixOption : ItemSixOptionTableViewCell?
    var mCellResultGeneric : ItemResultadoGenericTableViewCell?
    var mCellTwoOptionCustom : ItemTwoOptionCustomTableViewCell?
    var mCellElevenOptionCell : ItemElevenOptionTableViewCell?
    var mIndexPath : IndexPath?
    static var mAO : Int = 0
    static var mRV : Int = 0
    static var mRM : Int = 0
    static var mREN : Int = 0
    static var mNameRass : String = ""
    static var mRass : String = ""
    var mViewController : UIViewController?
    static var mEF : Int = 0
    static var mMC : Int = 0
    static var mTM : Int = 0
    static var mAV : Int = 0
    static var mNOEvAL : [String] = ["","","","","","","",""]
    
    static var mINICIO : [String] = ["","","","","","","","","","","","","","","","","","",""]
    static var mFIN : [String] = ["","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""]
    
    static var mACO : Int = 0
    static var mMIR : Int = 0
    static var mABL : Int = 0
    static var mACD : Int = 0
    static var mLC5 : Int = 0
    
    static var mEleven : Int = 0
    static var mCamFalse1 : Bool = false
    static var mCamFalse2 : Bool = false

    
    
    init(tableView: UITableView,mHiddenViewDelegate: HiddenViewDelegate, mViewController: UIViewController) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        self.tableView?.rowHeight = UITableView.automaticDimension
        self.mHiddenViewDelegate = mHiddenViewDelegate
        self.mViewController = mViewController
        //self.mAnswerDelegate = mAnswerDelegate
    }
    
    func update(items: [Question]) {
        self.items = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print("==>",items[indexPath.row].mAnswer[0].mID)
        if(self.items[indexPath.row].mTypeContainer == .ThreeOptionsHorizontal && self.items[indexPath.row].mAnswer[0].mID != "SSQ_ONE" && self.items[indexPath.row].mAnswer[0].mID != "SSQ_TWO" && self.items[indexPath.row].mAnswer[0].mID != "SSQ_THREE" &&
            self.items[indexPath.row].mAnswer[0].mID != "SSQ_FOUR" &&
            self.items[indexPath.row].mAnswer[0].mID != "SSQ_FIVE" &&
            self.items[indexPath.row].mAnswer[0].mID != "FA"){
            return 90
        }else if(self.items[indexPath.row].mTypeContainer == .TwoOptionsHorizontal && self.items[indexPath.row].mAnswer[0].mID != "SSQ_ONE" && self.items[indexPath.row].mAnswer[0].mID != "SSQ_TWO" && self.items[indexPath.row].mAnswer[0].mID != "SSQ_THREE" &&
        self.items[indexPath.row].mAnswer[0].mID != "SSQ_FOUR" &&
        self.items[indexPath.row].mAnswer[0].mID != "SSQ_FIVE" &&
        self.items[indexPath.row].mAnswer[0].mID != "FA"){
            return 90
        } else if(self.items[indexPath.row].mTypeContainer == .ListOption){
            return 40
        }else if(self.items[indexPath.row].mTypeContainer == .InputOptions){
            return 70
        }else if (self.items[indexPath.row].mTypeContainer == .FourOptionsVertical && items[indexPath.row].mAnswer.count == 3){
            return 200
        }else if(self.items[indexPath.row].mTypeContainer == .ThreeOtionVertical){
            return 170
        }else if(self.items[indexPath.row].mTypeContainer == .InputOptionFormulas){
            return 280
        }else if(self.items[indexPath.row].mTypeContainer == .ResultOption){
            return 150
        }else if(self.items[indexPath.row].mTypeContainer == .BannerOption){
            return 20
        }else if(self.items[indexPath.row].mTypeContainer == .SixOptionsVertical){
            return 300
        }else if(self.items[indexPath.row].mTypeContainer == .GenericResult){
            return 120
        }else if(self.items[indexPath.row].mTypeContainer == .GenericResult){
            return 250
        }else if(self.items[indexPath.row].mTypeContainer == .ItemElevenOptionVertical){
            return 480
        }else if(self.items[indexPath.row].mTypeContainer == .TwoOptionsHCustom){
            return 55
        }else if(self.items[indexPath.row]).mTypeContainer == .TwoOptionCustom{
            return 300
        }else if (self.items[indexPath.row].mTypeContainer == .FourOptionsVertical && items[indexPath.row].mAnswer.count == 4){
            return 230
        }else if(self.items[indexPath.row].mTypeContainer == .TwoOptionsHorizontal || self.items[indexPath.row].mTypeContainer == .ThreeOptionsHorizontal &&
            self.items[indexPath.row].mAnswer[0].mID == "FA" &&
            self.items[indexPath.row].mAnswer[0].mID != "SSQ_ONE" && self.items[indexPath.row].mAnswer[0].mID != "SSQ_TWO" && self.items[indexPath.row].mAnswer[0].mID != "SSQ_THREE" &&
            self.items[indexPath.row].mAnswer[0].mID != "SSQ_FOUR" &&
            self.items[indexPath.row].mAnswer[0].mID != "SSQ_FIVE"){
            print("REx","puto")
            return 140
        }else{
            return 250
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.items[indexPath.row]
        
        
        switch item.mTypeContainer {
        case .TwoOptionsHorizontal:
            let nib = UINib(nibName: mTwoOptionCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mTwoOptionCell)
            mCellTwoOption = (tableView.dequeueReusableCell(withIdentifier: mTwoOptionCell) as! ItemTwoOptionTableViewCell)
            mCellTwoOption?.mQuestionLabel.text = item.mQuestion
            mCellTwoOption?.mAnswer1Label.text = item.mAnswer[0].mAnswer
            mCellTwoOption?.mAnswer2Label.text = item.mAnswer[1].mAnswer
            
            mCellTwoOption?.mOption1Button.tag = indexPath.row
            mCellTwoOption?.mOption2Button.tag = indexPath.row
            
            mCellTwoOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
            mCellTwoOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
            
            if(item.mAnswer[0].mSelected){
                mCellTwoOption?.mIconOption1ImageView.image = UIImage(named: "ic_check")
                mCellTwoOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[1].mSelected){
                mCellTwoOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellTwoOption?.mIconOption2ImageView.image = UIImage(named: "ic_check")
            }
            
            mCellTwoOption?.mOption1Button.addTarget(self, action: #selector(TwoOption1Button(_:)), for: .touchUpInside)
            mCellTwoOption?.mOption2Button.addTarget(self, action: #selector(TwoOption2Button(_:)), for: .touchUpInside)
            
            mCell = mCellTwoOption
            
            break
            
        case .TwoOptionsHCustom:
            let nib = UINib(nibName: mTwoOptionCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mTwoOptionCell)
            mCellTwoOption = (tableView.dequeueReusableCell(withIdentifier: mTwoOptionCell) as! ItemTwoOptionTableViewCell)
            mCellTwoOption?.mQuestionLabel.text = item.mQuestion
            mCellTwoOption?.mAnswer1Label.text = item.mAnswer[0].mAnswer
            mCellTwoOption?.mAnswer2Label.text = item.mAnswer[1].mAnswer
            
            mCellTwoOption?.mOption1Button.tag = indexPath.row
            mCellTwoOption?.mOption2Button.tag = indexPath.row
            
            mCellTwoOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
            mCellTwoOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
            
            if(item.mAnswer[0].mSelected){
                mCellTwoOption?.mIconOption1ImageView.image = UIImage(named: "ic_check")
                mCellTwoOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[1].mSelected){
                mCellTwoOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellTwoOption?.mIconOption2ImageView.image = UIImage(named: "ic_check")
            }
            
            mCellTwoOption?.mOption1Button.addTarget(self, action: #selector(TwoOption1Button(_:)), for: .touchUpInside)
            mCellTwoOption?.mOption2Button.addTarget(self, action: #selector(TwoOption2Button(_:)), for: .touchUpInside)
            
            mCell = mCellTwoOption
            break
        case .ThreeOptionsHorizontal:
            let nib = UINib(nibName: mThreeOptionHorizonalCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mThreeOptionHorizonalCell)
            mCellThreeOption = (tableView.dequeueReusableCell(withIdentifier: mThreeOptionHorizonalCell) as! ItemThreeOptionsHorizontalTableViewCell)
            
            mCellThreeOption?.mQuestionLabel.text = item.mQuestion
            mCellThreeOption?.mAnswer1Label.text = item.mAnswer[0].mAnswer
            mCellThreeOption?.mAnswer2Label.text = item.mAnswer[1].mAnswer
            mCellThreeOption?.mAnswer3Label.text = item.mAnswer[2].mAnswer
            
            mCellThreeOption?.mOption1Button.tag = indexPath.row
            mCellThreeOption?.mOption2Button.tag = indexPath.row
            mCellThreeOption?.mOption3Button.tag = indexPath.row
            
            mCellThreeOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
            mCellThreeOption?.mIconOption2ImgaView.image = UIImage(named: "ic_uncheck")
            mCellThreeOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
            
            if(item.mAnswer[0].mSelected){
                mCellThreeOption?.mIconOption1ImageView.image = UIImage(named: "ic_check")
                mCellThreeOption?.mIconOption2ImgaView.image = UIImage(named: "ic_uncheck")
                mCellThreeOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                if(item.mAnswer[0].mID == "AO"){
                    AnswerDataSource.mAO = item.mAnswer[0].mValue.toInt()!
                }
                
                if(item.mAnswer[0].mID == "E_ONE"){
                    AnswerDataSource.mNOEvAL[0] = "1"
                }
                
                if(item.mAnswer[0].mID == "E_TWO"){
                    AnswerDataSource.mNOEvAL[1] = "1"
                }
                
                if(item.mAnswer[0].mID == "E_THREE"){
                    AnswerDataSource.mNOEvAL[2] = "1"
                }
                
                if(item.mAnswer[0].mID == "E_FOUR"){
                    AnswerDataSource.mNOEvAL[3] = "1"
                }
                
                if(item.mAnswer[0].mID == "E_FIVE"){
                    AnswerDataSource.mNOEvAL[4] = "1"
                }
                
                if(item.mAnswer[0].mID == "E_SIX"){
                    AnswerDataSource.mNOEvAL[5] = "1"
                }
                
                if(item.mAnswer[0].mID == "E_SEVEN"){
                    AnswerDataSource.mNOEvAL[6] = "1"
                }
                
                if(item.mAnswer[0].mID == "E_EIGHT"){
                    AnswerDataSource.mNOEvAL[7] = "1"
                }
            }
            
            if(item.mAnswer[1].mSelected){
                mCellThreeOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellThreeOption?.mIconOption2ImgaView.image = UIImage(named: "ic_check")
                mCellThreeOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                
                if(item.mAnswer[1].mID == "AO"){
                    AnswerDataSource.mAO = item.mAnswer[1].mValue.toInt()!
                }
                
                if(item.mAnswer[1].mID == "E_ONE"){
                    AnswerDataSource.mNOEvAL[0] = "0"
                }
                
                if(item.mAnswer[1].mID == "E_TWO"){
                    AnswerDataSource.mNOEvAL[1] = "0"
                }
                
                if(item.mAnswer[1].mID == "E_THREE"){
                    AnswerDataSource.mNOEvAL[2] = "0"
                }
                
                if(item.mAnswer[1].mID == "E_FOUR"){
                    AnswerDataSource.mNOEvAL[3] = "0"
                }
                
                if(item.mAnswer[1].mID == "E_FIVE"){
                    AnswerDataSource.mNOEvAL[4] = "0"
                }
                
                if(item.mAnswer[1].mID == "E_SIX"){
                    AnswerDataSource.mNOEvAL[5] = "0"
                }
                
                if(item.mAnswer[1].mID == "E_SEVEN"){
                    AnswerDataSource.mNOEvAL[6] = "0"
                }
                
                if(item.mAnswer[1].mID == "E_EIGHT"){
                    AnswerDataSource.mNOEvAL[7] = "0"
                }
            }
            
            if(item.mAnswer[2].mSelected){
                mCellThreeOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellThreeOption?.mIconOption2ImgaView.image = UIImage(named: "ic_uncheck")
                mCellThreeOption?.mIconOption3ImageView.image = UIImage(named: "ic_check")
                if(item.mAnswer[2].mID == "AO"){
                    AnswerDataSource.mAO = item.mAnswer[2].mValue.toInt()!
                }
                
                if( item.mAnswer[2].mID == "E_ONE"){
                    AnswerDataSource.mNOEvAL[0] = "1"
                }
                
                if(item.mAnswer[2].mID == "E_TWO"){
                    AnswerDataSource.mNOEvAL[1] = "1"
                    
                }
                
                if(item.mAnswer[2].mID == "E_THREE"){
                    AnswerDataSource.mNOEvAL[2] = "1"
                }
                
                if(item.mAnswer[2].mID == "E_FOUR"){
                    AnswerDataSource.mNOEvAL[3] = "1"
                }
                
                if(item.mAnswer[2].mID == "E_FIVE"){
                    AnswerDataSource.mNOEvAL[4] = "1"
                }
                
                if(item.mAnswer[2].mID == "E_SIX"){
                    AnswerDataSource.mNOEvAL[5] = "1"
                }
                
                if(item.mAnswer[2].mID == "E_SEVEN"){
                    AnswerDataSource.mNOEvAL[6] = "1"
                }
                
                if(item.mAnswer[2].mID == "E_EIGHT"){
                    AnswerDataSource.mNOEvAL[7] = "1"
                }
            }
            
            mCellThreeOption?.mOption1Button.addTarget(self, action: #selector(ThreeOptionH1Button(_:)), for: .touchUpInside)
            mCellThreeOption?.mOption2Button.addTarget(self, action: #selector(ThreeOptionH2Button(_:)), for: .touchUpInside)
            mCellThreeOption?.mOption3Button.addTarget(self, action: #selector(ThreeOptionH3Button(_:)), for: .touchUpInside)
            
            mCell = mCellThreeOption
            
            break
            
        case .FourOptionsVertical:
            let nib = UINib(nibName: mFourOptionVerticalCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mFourOptionVerticalCell)
            mCellFourOption = (tableView.dequeueReusableCell(withIdentifier: mFourOptionVerticalCell) as! ItemFourVerticalOptionsTableViewCell)
            mCellFourOption!.mQuestionLabel.text = item.mQuestion
            
            switch item.mAnswer.count {
            case 5:
                mCellFourOption?.mAnswer1Label.text = item.mAnswer[0].mAnswer
                mCellFourOption?.mAnswer2Label.text = item.mAnswer[1].mAnswer
                mCellFourOption?.mAnswer3Label.text = item.mAnswer[2].mAnswer
                mCellFourOption?.mAnswer4Label.text =  item.mAnswer[3].mAnswer
                mCellFourOption?.mAnswer5Label.text =  item.mAnswer[4].mAnswer
                
                mCellFourOption?.mOption1Button.tag = indexPath.row
                mCellFourOption?.mOption2Button.tag = indexPath.row
                mCellFourOption?.mOption3Button.tag = indexPath.row
                mCellFourOption?.mOption4Button.tag = indexPath.row
                mCellFourOption?.mOption5Button.tag = indexPath.row
                
                mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                
                if(item.mAnswer[0].mSelected){
                    mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_check")
                    mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                    if(item.mAnswer[0].mID == "RP"){
                        AnswerDataSource.mRV = item.mAnswer[0].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[0].mID == "AO"){
                        AnswerDataSource.mAO = item.mAnswer[0].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[0].mID == "EM"){
                        AnswerDataSource.mRass = item.mAnswer[0].mValue
                        AnswerDataSource.mNameRass = "Sedación"
                        
                    }
                    
                }
                
                if(item.mAnswer[1].mSelected){
                    mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_check")
                    mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                    if(item.mAnswer[1].mID == "RP"){
                        AnswerDataSource.mRV = item.mAnswer[1].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[1].mID == "AO"){
                        AnswerDataSource.mAO = item.mAnswer[1].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[1].mID == "EM"){
                        AnswerDataSource.mRass = item.mAnswer[1].mValue
                        AnswerDataSource.mNameRass = "Sedación"
                    }
                    
                }
                
                if(item.mAnswer[2].mSelected){
                    mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_check")
                    mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                    if(item.mAnswer[2].mID == "RP"){
                        AnswerDataSource.mRV = item.mAnswer[2].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[2].mID == "AO"){
                        AnswerDataSource.mAO = item.mAnswer[3].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[2].mID == "EM"){
                        AnswerDataSource.mRass = item.mAnswer[2].mValue
                        AnswerDataSource.mNameRass = "Sedación"
                    }

                }
                
                if(item.mAnswer[3].mSelected){
                    mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_check")
                    mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                    if(item.mAnswer[3].mID == "RP"){
                        AnswerDataSource.mRV = item.mAnswer[3].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[3].mID == "AO"){
                        AnswerDataSource.mAO = item.mAnswer[3].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[3].mID == "EM"){
                        AnswerDataSource.mRass = item.mAnswer[3].mValue
                        AnswerDataSource.mNameRass = "Sedación"
                    }
                    
                }
                
                if(item.mAnswer[4].mSelected){
                    mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_check")
                    if(item.mAnswer[4].mID == "RP"){
                        AnswerDataSource.mRV = item.mAnswer[4].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[4].mID == "AO"){
                        AnswerDataSource.mAO = item.mAnswer[4].mValue.toInt()!
                    }
                    
                    
                    if(item.mAnswer[4].mID == "EM"){
                        AnswerDataSource.mRass = item.mAnswer[4].mValue
                        AnswerDataSource.mNameRass = "Sedación"
                    }
                
                }
                
                mCellFourOption?.mOption1Button.addTarget(self, action: #selector(FiveOptionsV1Button(_:)), for: .touchUpInside)
                mCellFourOption?.mOption2Button.addTarget(self, action: #selector(FiveOptionsV2Button(_:)), for: .touchUpInside)
                mCellFourOption?.mOption3Button.addTarget(self, action: #selector(FiveOptionsV3Button(_:)), for: .touchUpInside)
                mCellFourOption?.mOption4Button.addTarget(self, action: #selector(FiveOptionsV4Button(_:)), for: .touchUpInside)
                mCellFourOption?.mOption5Button.addTarget(self, action: #selector(FiveOptionsV5Button(_:)), for: .touchUpInside)
                
                mCellFourOption?.mContainerOption4View.isHidden = false
                mCellFourOption?.mContainerOption5View.isHidden = false
                break
            case 4:
                mCellFourOption?.mAnswer1Label.text = item.mAnswer[0].mAnswer
                mCellFourOption?.mAnswer2Label.text = item.mAnswer[1].mAnswer
                mCellFourOption?.mAnswer3Label.text = item.mAnswer[2].mAnswer
                mCellFourOption?.mAnswer4Label.text = item.mAnswer[3].mAnswer
                
                mCellFourOption?.mOption1Button.tag = indexPath.row
                mCellFourOption?.mOption2Button.tag = indexPath.row
                mCellFourOption?.mOption3Button.tag = indexPath.row
                mCellFourOption?.mOption4Button.tag = indexPath.row
                mCellFourOption?.mOption5Button.tag = indexPath.row
                
                mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                
                if(item.mAnswer[0].mSelected){
                    mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_check")
                    mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                    
                    if(item.mAnswer[0].mID == "AN"){
                        AnswerDataSource.mRass = item.mAnswer[0].mValue
                        AnswerDataSource.mNameRass = "Agitacion"
                    }
                    
                    if(item.mAnswer[0].mID == "RP"){
                        AnswerDataSource.mRV = item.mAnswer[0].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[0].mID == "AO"){
                        AnswerDataSource.mAO = item.mAnswer[0].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[0].mID == "EM"){
                        AnswerDataSource.mRass = item.mAnswer[0].mValue
                        AnswerDataSource.mNameRass = "Sedación"
                    }
                    
                   
                }
                
                if(item.mAnswer[1].mSelected){
                    mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_check")
                    mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                    
                    if(item.mAnswer[1].mID == "RP"){
                        AnswerDataSource.mRV = item.mAnswer[1].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[1].mID == "AO"){
                        AnswerDataSource.mAO = item.mAnswer[1].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[1].mID == "EM"){
                        AnswerDataSource.mRass = item.mAnswer[1].mValue
                        AnswerDataSource.mNameRass = "Sedación"
                    }
                    
                    if(item.mAnswer[1].mID == "AN"){
                        AnswerDataSource.mRass = item.mAnswer[1].mValue
                        AnswerDataSource.mNameRass = "Agitacion"
                    }
                }
                
                if(item.mAnswer[2].mSelected){
                    mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_check")
                    mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                    if(item.mAnswer[2].mID == "RP"){
                        AnswerDataSource.mRV = item.mAnswer[2].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[2].mID == "AO"){
                        AnswerDataSource.mAO = item.mAnswer[2].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[2].mID == "EM"){
                        AnswerDataSource.mRass = item.mAnswer[2].mValue
                        AnswerDataSource.mNameRass = "Sedación"

                    }
                    
                    if(item.mAnswer[2].mID == "AN"){
                        AnswerDataSource.mRass = item.mAnswer[2].mValue
                        AnswerDataSource.mNameRass = "Agitacion"
                    }
                }
                
                if(item.mAnswer[3].mSelected){
                    mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                    mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_check")
                    mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                    
                    if(item.mAnswer[3].mID == "RP"){
                        AnswerDataSource.mRV = item.mAnswer[3].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[3].mID == "AO"){
                        AnswerDataSource.mAO = item.mAnswer[3].mValue.toInt()!
                    }
                    
                    if(item.mAnswer[3].mID == "EM"){
                        AnswerDataSource.mRass = item.mAnswer[3].mValue
                        AnswerDataSource.mNameRass = "Sedación"
                    }
                    
                    if(item.mAnswer[3].mID == "AN"){
                        AnswerDataSource.mRass = item.mAnswer[3].mValue
                        AnswerDataSource.mNameRass = "Agitacion"
                    }
                    
                }
            
                mCellFourOption?.mOption1Button.addTarget(self, action: #selector(FiveOptionsV1Button(_:)), for: .touchUpInside)
                mCellFourOption?.mOption2Button.addTarget(self, action: #selector(FiveOptionsV2Button(_:)), for: .touchUpInside)
                mCellFourOption?.mOption3Button.addTarget(self, action: #selector(FiveOptionsV3Button(_:)), for: .touchUpInside)
                mCellFourOption?.mOption4Button.addTarget(self, action: #selector(FiveOptionsV4Button(_:)), for: .touchUpInside)
                mCellFourOption?.mOption5Button.addTarget(self, action: #selector(FiveOptionsV5Button(_:)), for: .touchUpInside)
                
                mCellFourOption?.mContainerOption4View.isHidden = false
                mCellFourOption?.mContainerOption5View.isHidden = true
                break
                
            default:
                break
            }
            
            mCell = mCellFourOption
            break
            
        case .InputOptions:
            let nib = UINib(nibName: mInputOptionsCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mInputOptionsCell)
            mCellInputOption = (tableView.dequeueReusableCell(withIdentifier: mInputOptionsCell) as! ItemInputOptionTableViewCell)
            mCellInputOption?.mQuestionLabel.text = item.mQuestion
            mCellInputOption?.mAnswerTextField.placeholder = item.mAnswer[0].mAnswer
            
            mCell = mCellInputOption
            break
            
        case .ListOption:
            let nib = UINib(nibName: mListOptionCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mListOptionCell)
            mCellListOption = (tableView.dequeueReusableCell(withIdentifier: mListOptionCell) as! ItemListOptionTableViewCell)
            mCellListOption?.mOptionLabel.text = item.mQuestion
            mCell = mCellListOption
            break
            
        case .ThreeOtionVertical:
            let nib = UINib(nibName: mThreeOptipnVerticalCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mThreeOptipnVerticalCell)
            mCellThreeVerticalOption = (tableView.dequeueReusableCell(withIdentifier: mThreeOptipnVerticalCell) as! ItemThreeVerticalOptionsTableViewCell)
            mCellThreeVerticalOption?.mAnswer1Label.text = item.mAnswer[0].mAnswer
            mCellThreeVerticalOption?.mAnswer2Label.text = item.mAnswer[1].mAnswer
            mCellThreeVerticalOption?.mAnswer3Label.text = item.mAnswer[2].mAnswer
            mCellThreeVerticalOption?.mQuestionLabel.text = item.mQuestion
            
            mCellThreeVerticalOption?.mOptionButton1.tag = indexPath.row
            mCellThreeVerticalOption?.mOptionButton2.tag = indexPath.row
            mCellThreeVerticalOption?.mOptionButton3.tag = indexPath.row
            
            mCellThreeVerticalOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
            mCellThreeVerticalOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
            mCellThreeVerticalOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
            
            if(item.mAnswer[0].mSelected){
                mCellThreeVerticalOption?.mIconOption1ImageView.image = UIImage(named: "ic_check")
                mCellThreeVerticalOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellThreeVerticalOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                
                if(item.mAnswer[0].mID == "EF"){
                    AnswerDataSource.mEF = item.mAnswer[0].mValue.toInt()!
                }
                
                if(item.mAnswer[0].mID == "MC"){
                    AnswerDataSource.mMC = item.mAnswer[0].mValue.toInt()!
                }
                
                if(item.mAnswer[0].mID == "TM"){
                    AnswerDataSource.mTM = item.mAnswer[0].mValue.toInt()!
                }
                
                if(item.mAnswer[0].mID == "AV"){
                    AnswerDataSource.mAV = item.mAnswer[0].mValue.toInt()!
                }
            }
            
            if(item.mAnswer[1].mSelected){
                mCellThreeVerticalOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellThreeVerticalOption?.mIconOption2ImageView.image = UIImage(named: "ic_check")
                mCellThreeVerticalOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                
                if(item.mAnswer[1].mID == "EF"){
                    AnswerDataSource.mEF = item.mAnswer[1].mValue.toInt()!
                }
                
                if(item.mAnswer[1].mID == "MC"){
                    AnswerDataSource.mMC = item.mAnswer[1].mValue.toInt()!
                }
                
                if(item.mAnswer[1].mID == "TM"){
                    AnswerDataSource.mTM = item.mAnswer[1].mValue.toInt()!
                }
                
                if(item.mAnswer[1].mID == "AV"){
                    AnswerDataSource.mAV = item.mAnswer[1].mValue.toInt()!
                }
            }
            
            if(item.mAnswer[2].mSelected){
                mCellThreeVerticalOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellThreeVerticalOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellThreeVerticalOption?.mIconOption3ImageView.image = UIImage(named: "ic_check")
                
                if(item.mAnswer[2].mID == "EF"){
                    AnswerDataSource.mEF = item.mAnswer[2].mValue.toInt()!
                }
                
                if(item.mAnswer[2].mID == "MC"){
                    AnswerDataSource.mMC = item.mAnswer[2].mValue.toInt()!
                }
                
                if(item.mAnswer[2].mID == "TM"){
                    AnswerDataSource.mTM = item.mAnswer[2].mValue.toInt()!
                }
                
                if(item.mAnswer[2].mID == "AV"){
                    AnswerDataSource.mAV = item.mAnswer[2].mValue.toInt()!
                }
            }
            
            mCellThreeVerticalOption?.mOptionButton1.addTarget(self, action: #selector(ThreeOptionV1Button(_:)), for: .touchUpInside)
            mCellThreeVerticalOption?.mOptionButton2.addTarget(self, action: #selector(ThreeOptionV2Button(_:)), for: .touchUpInside)
            mCellThreeVerticalOption?.mOptionButton3.addTarget(self, action: #selector(ThreeOptionV3Button(_:)), for: .touchUpInside)
            
            mCell = mCellThreeVerticalOption
            break
            
        case .InputOptionFormulas :
            let nib = UINib(nibName: mInputFormulasOptionCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mInputFormulasOptionCell)
            mCellInputFormulasOption = (tableView.dequeueReusableCell(withIdentifier: mInputFormulasOptionCell) as! ItemFormulasTableViewCell)
            mCell = mCellInputFormulasOption
            
        case .ResultOption:
            let nib = UINib(nibName: mResultOptionCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mResultOptionCell)
            mCellResultOption = (tableView.dequeueReusableCell(withIdentifier: mResultOptionCell) as! ItemResultadoTableViewCell)
            
            if(item.mAnswer[0].mID == "RESCAM"){
                mCellResultOption?.mAnswer1.text = " "
                let mTotal = AnswerDataSource.mEF + AnswerDataSource.mMC + AnswerDataSource.mTM + AnswerDataSource.mAV
                mCellResultOption?.mAnswer2.text = mTotal.toString()
                mCellResultOption?.mAnswer4.text = "* Menor o igual a dos. No dolor signitficativo "
                mCellResultOption?.mAnswer5.text = "* Mayor a 2 Dolor considerable, se debe buscar alternativas de sedoanalgesia "
                mCellResultOption?.mAnswer3.text = ""
            }else if(item.mAnswer[0].mID == "RESEN"){
                mCellResultOption?.mAnswer1.text = "APERTURA OCULAR " + AnswerDataSource.mAO.toString()
                mCellResultOption?.mAnswer2.text = "RESPUESTA VERBAL " + AnswerDataSource.mRV.toString()
                mCellResultOption?.mAnswer3.text = "MEJOR RESPUESTA MOTORA " + AnswerDataSource.mRM.toString()
                mCellResultOption?.mAnswer4.text = ""
                
                AnswerDataSource.mREN = AnswerDataSource.mAO + AnswerDataSource.mRV + AnswerDataSource.mRM
                mCellResultOption?.mAnswer5.text = "TOTAL " + AnswerDataSource.mREN.toString()
            }else if(item.mAnswer[0].mID == "RESEVALNEU"){
                mCellResultOption?.mAnswer1.text = " "
                
                let mRes = AnswerDataSource.mACO + AnswerDataSource.mMIR + AnswerDataSource.mABL + AnswerDataSource.mACD + AnswerDataSource.mLC5
                mCellResultOption?.mAnswer2.text = mRes.toString()
                
                if(mRes > 2){
                    mCellResultOption?.mAnswer3.text = "PACIENTE COOPERATIVO Y APTO PARA EVALUAR MRC SUM SCORE"
                }else{
                    mCellResultOption?.mAnswer3.text = "PACIENTE NO COOPERATIVO Y NO APTO PARA EVALUAR MRC SUM SCORE"
                }
                
                mCellResultOption?.mAnswer4.text = ""
                mCellResultOption?.mAnswer5.font = mCellResultOption?.mAnswer5.font.withSize(9)
                mCellResultOption?.mAnswer5.text = "Considerar el el contexto clinico del paciente (ej. trauma facial parálisis facial, etc)"
            }
            mCell = mCellResultOption
            break
            
        case .BannerOption:
            let nib = UINib(nibName: mBannerOptionCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mBannerOptionCell)
            mCellBannerOption = (tableView.dequeueReusableCell(withIdentifier: mBannerOptionCell) as! ItemHeaderBannerTableViewCell)
            
            mCellBannerOption?.mIconImageView.image = UIImage(named: "ic_uncheck")
            
            if(item.mAnswer[0].mSelected){
                mCellBannerOption?.mIconImageView.image = UIImage(named: "ic_check")
                AnswerDataSource.mNameRass = "ALERTA Y TRANQUILO"
                AnswerDataSource.mRass = item.mAnswer[0].mValue
            }
            
            mCellBannerOption?.mBannerButton.addTarget(self, action: #selector(BannerOptionButton(_:)), for: .touchUpInside)
            mCell = mCellBannerOption
            
            break
            
        case .SixOptionsVertical:
            let nib = UINib(nibName: mSixOptionsVerticalCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mSixOptionsVerticalCell)
            mCellSixOption = (tableView.dequeueReusableCell(withIdentifier: mSixOptionsVerticalCell) as! ItemSixOptionTableViewCell)
            mCellSixOption?.mQuestionLabel.text = item.mQuestion
            
            mCellSixOption?.mAnswerOption1Label.text = item.mAnswer[0].mAnswer
            mCellSixOption?.mAnswerOption2Label.text = item.mAnswer[1].mAnswer
            mCellSixOption?.mAnswerOption3Label.text = item.mAnswer[2].mAnswer
            mCellSixOption?.mAnswerOption4Label.text =  item.mAnswer[3].mAnswer
            mCellSixOption?.mAnswerOption5Label.text =  item.mAnswer[4].mAnswer
            mCellSixOption?.mAnswerOption6Label.text =  item.mAnswer[5].mAnswer
            
            mCellSixOption?.mOption1Button.tag = indexPath.row
            mCellSixOption?.mOption2Button.tag = indexPath.row
            mCellSixOption?.mOption3Button.tag = indexPath.row
            mCellSixOption?.mOption4Button.tag = indexPath.row
            mCellSixOption?.mOption5Button.tag = indexPath.row
            mCellSixOption?.mOption6Button.tag = indexPath.row
            
            mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
            mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
            mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
            mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
            mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
            mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
            
            if(item.mAnswer[0].mSelected){
                mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_check")
                mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
                
                if(item.mAnswer[0].mID == "RM"){
                    AnswerDataSource.mRM = item.mAnswer[0].mValue.toInt()!
                }
                
            }
            
            if(item.mAnswer[1].mSelected){
                mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_check")
                mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
                
                if(item.mAnswer[1].mID == "RM"){
                    AnswerDataSource.mRM = item.mAnswer[1].mValue.toInt()!
                }
            }
            
            if(item.mAnswer[2].mSelected){
                mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_check")
                mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
                
                if(item.mAnswer[2].mID == "RM"){
                    AnswerDataSource.mRM = item.mAnswer[2].mValue.toInt()!
                }
            }
            
            if(item.mAnswer[3].mSelected){
                mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_check")
                mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
                
                if(item.mAnswer[3].mID == "RM"){
                    AnswerDataSource.mRM = item.mAnswer[3].mValue.toInt()!
                }
            }
            
            if(item.mAnswer[4].mSelected){
                mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_check")
                mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
                
                if(item.mAnswer[4].mID == "RM"){
                    AnswerDataSource.mRM = item.mAnswer[4].mValue.toInt()!
                }
            }
            
            if(item.mAnswer[5].mSelected){
                mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
                mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
                mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_check")
                
                if(item.mAnswer[5].mID == "RM"){
                    AnswerDataSource.mRM = item.mAnswer[5].mValue.toInt()!
                }
            }
            
            mCellSixOption?.mOption1Button.addTarget(self, action: #selector(SixOptionV1Button(_:)), for: .touchUpInside)
            mCellSixOption?.mOption2Button.addTarget(self, action: #selector(SixOptionV2Button(_:)), for: .touchUpInside)
            mCellSixOption?.mOption3Button.addTarget(self, action: #selector(SixOptionV3Button(_:)), for: .touchUpInside)
            mCellSixOption?.mOption4Button.addTarget(self, action: #selector(SixOptionV4Button(_:)), for: .touchUpInside)
            mCellSixOption?.mOption5Button.addTarget(self, action: #selector(SixOptionV5Button(_:)), for: .touchUpInside)
            mCellSixOption?.mOption6Button.addTarget(self, action: #selector(SixOptionV6Button(_:)), for: .touchUpInside)
            
            mCell = mCellSixOption
            break
        case .GenericResult:
            let nib = UINib(nibName: mResutlGenericCell, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mResutlGenericCell)
            mCellResultGeneric = (tableView.dequeueReusableCell(withIdentifier: mResutlGenericCell) as! ItemResultadoGenericTableViewCell)
            if(item.mAnswer[0].mID == "RESRASS"){
                mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mNameRass + " " +  AnswerDataSource.mRass
            }
            
            if(item.mAnswer[0].mID == "RESEVALU"){
                var mRes : String = ""
                for item in AnswerDataSource.mNOEvAL{
                    if(item == "1"){
                        mRes = "1"
                    }else if(item == "0"){
                        mRes = "0"
                        break
                    }else{
                        mRes = ""
                        break
                    }
                }
                
                if(mRes == "1"){
                    mCellResultGeneric?.mResultLabel.text = "PUEDE INICIAR LA MOVILIZACIÓN"
                }else if(mRes == "0"){
                    mCellResultGeneric?.mResultLabel.text = "NO SE PUEDED INICIAR LA MOVILIZACIÓN ESPERE AL SIGUIENTE TURNO"
                }else{
                    mCellResultGeneric?.mResultLabel.text = ""
                }
            }
            
            if(item.mAnswer[0].mID == "RESINICIO"){
                var mRes : String = ""
                for item in AnswerDataSource.mINICIO {
                    if(item == "1"){
                        mRes = "1"
                    }else if (item == "0"){
                        mRes = "0"
                        break
                    }else{
                        mRes = ""
                        break
                    }
                }
                
                
                if(mRes == "1"){
                    mCellResultGeneric?.mResultLabel.text = "ES SEGURO MOVILIZAR"
                }else if(mRes == "0"){
                    mCellResultGeneric?.mResultLabel.text = "CONSULTAR AL MÉDICO TRATANTE ANTES DE MOVILIZAR"
                }else{
                    mCellResultGeneric?.mResultLabel.text = ""
                }
            }
            
            if(item.mAnswer[0].mID == "RESFIN"){
                var mRes : String = ""
                for item in AnswerDataSource.mFIN {
                    if(item == "0"){
                        mRes = "0"
                    }else if(item == "1"){
                        mRes = "1"
                        break
                    }else{
                        mRes = ""
                        break
                    }
                }
                
                
                if(mRes == "0"){
                    mCellResultGeneric?.mResultLabel.text = "PUEDES INICIAR LA MOVILIZACIÓN"
                }else if(mRes == "1"){
                    mCellResultGeneric?.mResultLabel.text = "DEBE PARAR LA MOVILIZACIÓN"
                }else{
                    mCellResultGeneric?.mResultLabel.text = ""
                }
            }
            
            if(item.mAnswer[0].mID == "RESMOVILIDAD"){
                
                switch AnswerDataSource.mEleven.toString() {
                case "0":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Rodados pasivos, movilizaciones pasivas, NO movilización activa"
                    break
                case "1":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Cualquier actividad en cama, incluyendo rodados, ejercicios activos, cicloergómetro y movilizaciones activo asistidas, no movilización a la orilla de la cama"
                    break
                case "2":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Transferencia pasiva o por grúa a la silla, sin bipedestación o sentado a la orilla de la cama"
                    break
                case "3":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Puede tener ayuda del personal, pero involucra sentado activo sobre un costado de la cama con algo de control de tronco"
                    break
                case "4":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Sostiene su peso en posición bípeda, con o sin asistencia. Esto puede incluir el uso de una grúa o una tabla de verticalización"
                    break
                case "5":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Es capaz de dar pasos por el mismo a la silla"
                    break
                case "6":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Capaz de alternar el peso al menos dos veces en cada pierna (4 pasos)"
                    break
                case "7":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Caminar fuera de la cama/silla por lo menos 5 metros"
                    break
                case "8":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Caminar fuera de la cama/silla por lo menos 5 metros"
                    break
                case "9":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Caminar fuera de la cama/silla por lo menos 5 metros"
                    break
                case "10":
                    mCellResultGeneric?.mResultLabel.text = AnswerDataSource.mEleven.toString() + "- Caminar fuera de la cama/silla por lo menos 5 metros"
                    break
                default:
                    break
                }
            }
            
            mCell = mCellResultGeneric
            break
            
        case .TwoOptionCustom:
            let nib = UINib(nibName: mTwoOptionCustom, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mTwoOptionCustom)
            mCellTwoOptionCustom = (tableView.dequeueReusableCell(withIdentifier: mTwoOptionCustom) as! ItemTwoOptionCustomTableViewCell)
            
            
            mCellTwoOptionCustom?.mOption1Button.addTarget(self, action: #selector(TwoOptionCustom1Button(_:)), for: .touchUpInside)
            mCellTwoOptionCustom?.mOption2Button.addTarget(self, action: #selector(TwoOptionCustom2Button(_:)), for: .touchUpInside)
            
            mCellTwoOptionCustom?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
            mCellTwoOptionCustom?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
            
            if(item.mAnswer[0].mSelected){
                
                mCellTwoOptionCustom?.mIconOption1ImageView.image = UIImage(named: "ic_check")
                mCellTwoOptionCustom?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[1].mSelected){
                
                mCellTwoOptionCustom?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellTwoOptionCustom?.mIconOption2ImageView.image = UIImage(named: "ic_check")
            }
            
        
            mCell = mCellTwoOptionCustom
            
            break
            
        case .ItemElevenOptionVertical:
            let nib = UINib(nibName: mElevenOption, bundle: nil)
            self.tableView!.register(nib, forCellReuseIdentifier: mElevenOption)
            mCellElevenOptionCell = (tableView.dequeueReusableCell(withIdentifier: mElevenOption) as! ItemElevenOptionTableViewCell)
            
            mCellElevenOptionCell?.mOption1Button.addTarget(self, action: #selector(ElevenOption1Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption2Button.addTarget(self, action: #selector(ElevenOption2Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption3Button.addTarget(self, action: #selector(ElevenOption3Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption4Button.addTarget(self, action: #selector(ElevenOption4Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption5Button.addTarget(self, action: #selector(ElevenOption5Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption6Button.addTarget(self, action: #selector(ElevenOption6Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption7Button.addTarget(self, action: #selector(ElevenOption7Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption8Button.addTarget(self, action: #selector(ElevenOption8Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption9Button.addTarget(self, action: #selector(ElevenOption9Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption10Button.addTarget(self, action: #selector(ElevenOption10Button(_:)), for: .touchUpInside)
            mCellElevenOptionCell?.mOption11Button.addTarget(self, action: #selector(ElevenOption11Button(_:)), for: .touchUpInside)
            
            if(item.mAnswer[0].mSelected){
                AnswerDataSource.mEleven = 0
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[1].mSelected){
                AnswerDataSource.mEleven = 1
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[2].mSelected){
                AnswerDataSource.mEleven = 2
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[3].mSelected){
                AnswerDataSource.mEleven = 3
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[4].mSelected){
                AnswerDataSource.mEleven = 4
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[5].mSelected){
                AnswerDataSource.mEleven = 5
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[6].mSelected){
                AnswerDataSource.mEleven = 6
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[7].mSelected){
                AnswerDataSource.mEleven = 7
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[8].mSelected){
                AnswerDataSource.mEleven = 8
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[9].mSelected){
                AnswerDataSource.mEleven = 9
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_check")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_uncheck")
            }
            
            if(item.mAnswer[10].mSelected){
                AnswerDataSource.mEleven = 10
                mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named: "ic_uncheck")
                mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named: "ic_check")
            }
            
            
            mCell = mCellElevenOptionCell
            break
        default:
            break
        }
        
        return mCell!
    }
    
    
    //MARK:- Targets Two Option
    
    @objc func TwoOption1Button(_ sender : UIButton){
        
        items[sender.tag].mAnswer[0].mSelected = true
        items[sender.tag].mAnswer[1].mSelected = false
        mCellTwoOption = (tableView?.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! ItemTwoOptionTableViewCell)
        mCellTwoOption?.mIconOption1ImageView.image = UIImage(named: "ic_check")
        mCellTwoOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
        
        if(items[sender.tag].mAnswer[0].mID == "CM"){
            if(sender.tag == 1){
                if(items[sender.tag - 1].mAnswer[0].mSelected == true || items[sender.tag].mAnswer[0].mSelected == true){
                    mHiddenViewDelegate?.hideShowSection(mTypeSection: .FALTA_ATENCION, mShowSection: true)
                }
            }else{
                if(items[sender.tag + 1].mAnswer[0].mSelected == true || items[sender.tag].mAnswer[0].mSelected == true){
                    mHiddenViewDelegate?.hideShowSection(mTypeSection: .FALTA_ATENCION, mShowSection: true)
                }
            }
        }
        
        if(items[sender.tag].mAnswer[0].mID == "FA"){
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .NIVEL_DE_CONCIENCIA, mShowSection: true)
        }
        
        if(items[sender.tag].mAnswer[0].mID == "NC"){
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .PENSAMIENTO_DESORGANIZADO, mShowSection: true)
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_ONE"){
            AnswerDataSource.mACO = items[sender.tag].mAnswer[0].mValue.toInt()!
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_TWO"){
            AnswerDataSource.mMIR = items[sender.tag].mAnswer[0].mValue.toInt()!
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_THREE"){
            AnswerDataSource.mABL = items[sender.tag].mAnswer[0].mValue.toInt()!
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_FOUR"){
            AnswerDataSource.mACD = items[sender.tag].mAnswer[0].mValue.toInt()!
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_FIVE"){
            AnswerDataSource.mLC5 = items[sender.tag].mAnswer[0].mValue.toInt()!
        }
        
        
        /*=======================**/
        
        if(items[sender.tag].mAnswer[0].mID == "IN1"){
            AnswerDataSource.mINICIO[0] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "IN2"){
            AnswerDataSource.mINICIO[1] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "IN3"){
            AnswerDataSource.mINICIO[2] = items[sender.tag].mAnswer[0].mValue
        }
        
        
        if(items[sender.tag].mAnswer[0].mID == "IN4"){
            AnswerDataSource.mINICIO[3] = items[sender.tag].mAnswer[0].mValue
        }
        
        //**============**/
        
        if(items[sender.tag].mAnswer[0].mID == "CARD1"){
            AnswerDataSource.mINICIO[4] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD2"){
            AnswerDataSource.mINICIO[5] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD3"){
            AnswerDataSource.mINICIO[6] = items[sender.tag].mAnswer[0].mValue
        }
        
        
        if(items[sender.tag].mAnswer[0].mID == "CARD4"){
            AnswerDataSource.mINICIO[7] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD5"){
            AnswerDataSource.mINICIO[8] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD6"){
            AnswerDataSource.mINICIO[9] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD7"){
            AnswerDataSource.mINICIO[10] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD8"){
            AnswerDataSource.mINICIO[11] = items[sender.tag].mAnswer[0].mValue
        }
        /**==============================*/
        
        if(items[sender.tag].mAnswer[0].mID == "RESP1"){
            AnswerDataSource.mINICIO[12] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP2"){
            AnswerDataSource.mINICIO[13] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP3"){
            AnswerDataSource.mINICIO[14] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP4"){
            AnswerDataSource.mINICIO[15] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP5"){
            AnswerDataSource.mINICIO[16] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP6"){
            AnswerDataSource.mINICIO[17] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP7"){
            AnswerDataSource.mINICIO[18] = items[sender.tag].mAnswer[0].mValue
        }
        
        
        /*##############################################*/
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD1"){
            AnswerDataSource.mFIN[0] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD2"){
            AnswerDataSource.mFIN[1] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD3"){
            AnswerDataSource.mFIN[2] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD4"){
            AnswerDataSource.mFIN[3] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD5"){
            AnswerDataSource.mFIN[4] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD6"){
            AnswerDataSource.mFIN[5] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD7"){
            AnswerDataSource.mFIN[6] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD8"){
            AnswerDataSource.mFIN[7] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD9"){
            AnswerDataSource.mFIN[8] = items[sender.tag].mAnswer[0].mValue
        }
        
        /*##############################################*/
        
        if(items[sender.tag].mAnswer[0].mID == "NEUDD1"){
            AnswerDataSource.mFIN[9] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "NEUDD2"){
            AnswerDataSource.mFIN[10] = items[sender.tag].mAnswer[0].mValue
        }
        
        
        /*########################################**/
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD1"){
            AnswerDataSource.mFIN[11] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD2"){
            AnswerDataSource.mFIN[12] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD3"){
            AnswerDataSource.mFIN[13] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD4"){
            AnswerDataSource.mFIN[14] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD5"){
            AnswerDataSource.mFIN[15] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD6"){
            AnswerDataSource.mFIN[16] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD7"){
            AnswerDataSource.mFIN[17] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD8"){
            AnswerDataSource.mFIN[18] = items[sender.tag].mAnswer[0].mValue
        }
        
        
        /*########################################**/
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP1"){
            AnswerDataSource.mFIN[19] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP3"){
            AnswerDataSource.mFIN[20] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP5"){
            AnswerDataSource.mFIN[21] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP6"){
            AnswerDataSource.mFIN[22] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP7"){
            AnswerDataSource.mFIN[23] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP8"){
            AnswerDataSource.mFIN[24] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP9"){
            AnswerDataSource.mFIN[25] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP10"){
            AnswerDataSource.mFIN[26] = items[sender.tag].mAnswer[0].mValue
        }
        
        /*########################################**/
        
        if(items[sender.tag].mAnswer[0].mID == "GEN1"){
            AnswerDataSource.mFIN[27] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "GEN2"){
            AnswerDataSource.mFIN[28] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "GEN3"){
            AnswerDataSource.mFIN[29] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "GEN4"){
            AnswerDataSource.mFIN[30] = items[sender.tag].mAnswer[0].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "GEN5"){
            AnswerDataSource.mFIN[31] = items[sender.tag].mAnswer[0].mValue
        }
        
        self.tableView?.reloadData()
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func TwoOption2Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = true
        mCellTwoOption = (tableView?.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! ItemTwoOptionTableViewCell)
        mCellTwoOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
        mCellTwoOption?.mIconOption2ImageView.image = UIImage(named: "ic_check")
        
        if(items[sender.tag].mAnswer[0].mID == "CM"){
            if(sender.tag == 1){
                if(items[sender.tag - 1].mAnswer[1].mSelected == true && items[sender.tag].mAnswer[0].mSelected == false){
                    mHiddenViewDelegate?.hideShowSection(mTypeSection: .FALTA_ATENCION, mShowSection: false)
                    
                    mHiddenViewDelegate?.hideShowSection(mTypeSection: .NIVEL_DE_CONCIENCIA, mShowSection: false)
                    mHiddenViewDelegate?.hideShowSection(mTypeSection: .PENSAMIENTO_DESORGANIZADO, mShowSection: false)
                    
                    AnswerDataSource.mCamFalse1 = true
                    
                    AlertDialog.show(title: "Aviso", body: "EL PACIENTE NO TIENE DELIRIUM", view: mViewController!)
                    
                }
            }else{
                if(items[sender.tag + 1].mAnswer[1].mSelected == true && items[sender.tag].mAnswer[0].mSelected == false){
                    mHiddenViewDelegate?.hideShowSection(mTypeSection: .FALTA_ATENCION, mShowSection: false)
                    
                    mHiddenViewDelegate?.hideShowSection(mTypeSection: .NIVEL_DE_CONCIENCIA, mShowSection: false)
                
                    mHiddenViewDelegate?.hideShowSection(mTypeSection: .PENSAMIENTO_DESORGANIZADO, mShowSection: false)
                    
                     AnswerDataSource.mCamFalse2 = true
                    
                   // if(AnswerDataSource.mCamFalse1){
                         AlertDialog.show(title: "Aviso", body: "EL PACIENTE NO TIENE DELIRIUM", view: mViewController!)
                    //}
                }
            }
        }
        
        if(items[sender.tag].mAnswer[0].mID == "FA"){
            AlertDialog.show(title: "Aviso", body: "EL PACIENTE NO TIENE DELIRIUM", view: mViewController!)
            
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .NIVEL_DE_CONCIENCIA, mShowSection: false)
            
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_ONE"){
            AnswerDataSource.mACO = items[sender.tag].mAnswer[1].mValue.toInt()!
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_TWO"){
            AnswerDataSource.mMIR = items[sender.tag].mAnswer[1].mValue.toInt()!
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_THREE"){
            AnswerDataSource.mABL = items[sender.tag].mAnswer[1].mValue.toInt()!
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_FOUR"){
            AnswerDataSource.mACD = items[sender.tag].mAnswer[1].mValue.toInt()!
        }
        
        if(items[sender.tag].mAnswer[0].mID == "SSQ_FIVE"){
            AnswerDataSource.mLC5 = items[sender.tag].mAnswer[1].mValue.toInt()!
        }
        
        if(items[sender.tag].mAnswer[0].mID == "NC"){
            AlertDialog.show(title: "Aviso", body: "POSITIVO CON DELIRIUM", view: mViewController!)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .PENSAMIENTO_DESORGANIZADO, mShowSection: false)
        }
        
        /**===================================**/
        
        if(items[sender.tag].mAnswer[0].mID == "IN1"){
            AnswerDataSource.mINICIO[0] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "IN2"){
            AnswerDataSource.mINICIO[1] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "IN3"){
            AnswerDataSource.mINICIO[2] = items[sender.tag].mAnswer[1].mValue
        }
        
        
        if(items[sender.tag].mAnswer[0].mID == "IN4"){
            AnswerDataSource.mINICIO[3] = items[sender.tag].mAnswer[1].mValue
        }
        
        //**============**/
        
        if(items[sender.tag].mAnswer[0].mID == "CARD1"){
            AnswerDataSource.mINICIO[4] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD2"){
            AnswerDataSource.mINICIO[5] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD3"){
            AnswerDataSource.mINICIO[6] = items[sender.tag].mAnswer[1].mValue
        }
        
        
        if(items[sender.tag].mAnswer[0].mID == "CARD4"){
            AnswerDataSource.mINICIO[7] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD5"){
            AnswerDataSource.mINICIO[8] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD6"){
            AnswerDataSource.mINICIO[9] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD7"){
            AnswerDataSource.mINICIO[10] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARD8"){
            AnswerDataSource.mINICIO[11] = items[sender.tag].mAnswer[1].mValue
        }
        /**==============================*/
        
        if(items[sender.tag].mAnswer[0].mID == "RESP1"){
            AnswerDataSource.mINICIO[12] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP2"){
            AnswerDataSource.mINICIO[13] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP3"){
            AnswerDataSource.mINICIO[14] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP4"){
            AnswerDataSource.mINICIO[15] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP5"){
            AnswerDataSource.mINICIO[16] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP6"){
            AnswerDataSource.mINICIO[17] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESP7"){
            AnswerDataSource.mINICIO[18] = items[sender.tag].mAnswer[1].mValue
        }
        
        
        /*##############################################*/
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD1"){
            AnswerDataSource.mFIN[0] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD2"){
            AnswerDataSource.mFIN[1] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD3"){
            AnswerDataSource.mFIN[2] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD4"){
            AnswerDataSource.mFIN[3] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD5"){
            AnswerDataSource.mFIN[4] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD6"){
            AnswerDataSource.mFIN[5] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD7"){
            AnswerDataSource.mFIN[6] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD8"){
            AnswerDataSource.mFIN[7] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDD9"){
            AnswerDataSource.mFIN[8] = items[sender.tag].mAnswer[1].mValue
        }
        
        /*##############################################*/
        
        if(items[sender.tag].mAnswer[0].mID == "NEUDD1"){
            AnswerDataSource.mFIN[9] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "NEUDD2"){
            AnswerDataSource.mFIN[10] = items[sender.tag].mAnswer[1].mValue
        }
        
        
        /*########################################**/
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD1"){
            AnswerDataSource.mFIN[11] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD2"){
            AnswerDataSource.mFIN[12] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD3"){
            AnswerDataSource.mFIN[13] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD4"){
            AnswerDataSource.mFIN[14] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD5"){
            AnswerDataSource.mFIN[15] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD6"){
            AnswerDataSource.mFIN[16] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD7"){
            AnswerDataSource.mFIN[17] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "CARDDD8"){
            AnswerDataSource.mFIN[18] = items[sender.tag].mAnswer[1].mValue
        }
        
        
        /*########################################**/
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP1"){
            AnswerDataSource.mFIN[19] = items[sender.tag].mAnswer[1].mValue
        }
        
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP3"){
            AnswerDataSource.mFIN[20] = items[sender.tag].mAnswer[1].mValue
        }
        
       
        if(items[sender.tag].mAnswer[0].mID == "RESPP5"){
            AnswerDataSource.mFIN[21] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP6"){
            AnswerDataSource.mFIN[22] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP7"){
            AnswerDataSource.mFIN[23] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP8"){
            AnswerDataSource.mFIN[24] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP9"){
            AnswerDataSource.mFIN[25] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "RESPP10"){
            AnswerDataSource.mFIN[26] = items[sender.tag].mAnswer[1].mValue
        }
        
        /*########################################**/
        
        if(items[sender.tag].mAnswer[0].mID == "GEN1"){
            AnswerDataSource.mFIN[27] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "GEN2"){
            AnswerDataSource.mFIN[28] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "GEN3"){
            AnswerDataSource.mFIN[29] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "GEN4"){
            AnswerDataSource.mFIN[30] = items[sender.tag].mAnswer[1].mValue
        }
        
        if(items[sender.tag].mAnswer[0].mID == "GEN5"){
            AnswerDataSource.mFIN[31] = items[sender.tag].mAnswer[1].mValue
        }
        
        self.tableView?.reloadData()
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    //MARK:-  Targets Three Option Horiozontal
    
    @objc func ThreeOptionH1Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = true
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        
        mCellThreeOption?.mIconOption1ImageView.image = UIImage(named: "ic_check")
        mCellThreeOption?.mIconOption2ImgaView.image = UIImage(named: "ic_uncheck")
        mCellThreeOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ThreeOptionH2Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = true
        items[sender.tag].mAnswer[2].mSelected = false
        mCellThreeOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
        mCellThreeOption?.mIconOption2ImgaView.image = UIImage(named: "ic_check")
        mCellThreeOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ThreeOptionH3Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = true
        mCellThreeOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
        mCellThreeOption?.mIconOption2ImgaView.image = UIImage(named: "ic_uncheck")
        mCellThreeOption?.mIconOption3ImageView.image = UIImage(named: "ic_check")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    
    //MARK:- Five Option Vertical
    
    @objc func FiveOptionsV1Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = true
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        
        
        if(items[sender.tag].mAnswer.count == 5){
            items[sender.tag].mAnswer[4].mSelected = false
        }
        
        if(items[sender.tag].mAnswer[0].mID == "AO"){
            
        }
        
        if(items[sender.tag].mAnswer[0].mID == "AN"){
            mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Agitacion")
        }
        
        if(items[sender.tag].mAnswer[0].mID == "EM"){
            mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Sedacion")
        }
        mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_check")
        mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    @objc func FiveOptionsV2Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = true
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        if(items[sender.tag].mAnswer.count == 5){
            items[sender.tag].mAnswer[4].mSelected = false
        }
        
        if(items[sender.tag].mAnswer[1].mID == "AO"){
            
        }
        
        if(items[sender.tag].mAnswer[0].mID == "EM"){
            mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Sedacion")
        }
        
        if(items[sender.tag].mAnswer[0].mID == "AN"){
            mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Agitacion")
        }
        
        mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_check")
        mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func FiveOptionsV3Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = true
        items[sender.tag].mAnswer[3].mSelected = false
        if(items[sender.tag].mAnswer.count == 5){
            items[sender.tag].mAnswer[4].mSelected = false
        }
        
        if(items[sender.tag].mAnswer[0].mID == "EM"){
            mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Sedacion")
        }
        
        if(items[sender.tag].mAnswer[0].mID == "AN"){
            mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Agitacion")
        }
        
        mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_check")
        mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    @objc func FiveOptionsV4Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = true
        if(items[sender.tag].mAnswer.count == 5){
            items[sender.tag].mAnswer[4].mSelected = false
        }
        
        if(items[sender.tag].mAnswer[0].mID == "EM"){
            mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Sedacion")
        }
        
        if(items[sender.tag].mAnswer[0].mID == "AN"){
            mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Agitacion")
        }
        
        mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_check")
        mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func FiveOptionsV5Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        if(items[sender.tag].mAnswer.count == 5){
            items[sender.tag].mAnswer[4].mSelected = true
        }
        
        if(items[sender.tag].mAnswer[0].mID == "EM"){
            mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Sedacion")
        }
        
        if(items[sender.tag].mAnswer[0].mID == "AN"){
                   mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Agitacion")
               }
               
        
        mCellFourOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellFourOption?.mIconOption5ImageView.image = UIImage(named:"ic_check")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    //MARK:- Three Option Vertical
    
    @objc func ThreeOptionV1Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = true
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        
        if(items[sender.tag].mAnswer[0].mID == "GE"){
            AnswerDataSource.mAO = 0
            AnswerDataSource.mRV = 0
            AnswerDataSource.mRM = 0
            AnswerDataSource.mREN = 0
            mHiddenViewDelegate?.OnSelectEDAD(mEdad: items[sender.tag].mAnswer[0].mAnswer)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .APERTURA_OCULAR, mShowSection: true)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .RESPUESTA_VERBAL, mShowSection: true)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .RESPUESTA_MOTORA, mShowSection: true)
            
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .RESULTADO_EN, mShowSection: true)
        }
        
        mCellThreeVerticalOption?.mIconOption1ImageView.image = UIImage(named: "ic_check")
        mCellThreeVerticalOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
        mCellThreeVerticalOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ThreeOptionV2Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = true
        items[sender.tag].mAnswer[2].mSelected = false
        
        if(items[sender.tag].mAnswer[0].mID == "GE"){
            AnswerDataSource.mAO = 0
            AnswerDataSource.mRV = 0
            AnswerDataSource.mRM = 0
            AnswerDataSource.mREN = 0
            mHiddenViewDelegate?.OnSelectEDAD(mEdad: items[sender.tag].mAnswer[1].mAnswer)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .APERTURA_OCULAR, mShowSection: true)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .RESPUESTA_VERBAL, mShowSection: true)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .RESPUESTA_MOTORA, mShowSection: true)
            
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .RESULTADO_EN, mShowSection: true)
            

        }
        
        mCellThreeVerticalOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
        mCellThreeVerticalOption?.mIconOption2ImageView.image = UIImage(named: "ic_check")
        mCellThreeVerticalOption?.mIconOption3ImageView.image = UIImage(named: "ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ThreeOptionV3Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = true
        
        if(items[sender.tag].mAnswer[0].mID == "GE"){
            AnswerDataSource.mAO = 0
            AnswerDataSource.mRV = 0
            AnswerDataSource.mRM = 0
            AnswerDataSource.mREN = 0
            mHiddenViewDelegate?.OnSelectEDAD(mEdad: items[sender.tag].mAnswer[2].mAnswer)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .APERTURA_OCULAR, mShowSection: true)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .RESPUESTA_VERBAL, mShowSection: true)
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .RESPUESTA_MOTORA, mShowSection: true)
            
            mHiddenViewDelegate?.hideShowSection(mTypeSection: .RESULTADO_EN, mShowSection: true)
        }
        
        mCellThreeVerticalOption?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
        mCellThreeVerticalOption?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
        mCellThreeVerticalOption?.mIconOption3ImageView.image = UIImage(named: "ic_check")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    //MARK:- Six Option Vertical
    
    @objc func SixOptionV1Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = true
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        
        mCellSixOption?.mIconOption1ImageView.image = UIImage(named:"ic_check")
        mCellSixOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func SixOptionV2Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = true
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        
        mCellSixOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption2ImageView.image = UIImage(named:"ic_check")
        mCellSixOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func SixOptionV3Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = true
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        mCellSixOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption3ImageView.image = UIImage(named:"ic_check")
        mCellSixOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    @objc func SixOptionV4Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = true
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        mCellSixOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption4ImageView.image = UIImage(named:"ic_check")
        mCellSixOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func SixOptionV5Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = true
        items[sender.tag].mAnswer[5].mSelected = false
        mCellSixOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption5ImageView.image = UIImage(named:"ic_check")
        mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    @objc func SixOptionV6Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = true
        mCellSixOption?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellSixOption?.mIconOption6ImageView.image = UIImage(named:"ic_check")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    //MARK:- Banner Button Option
    
    @objc func BannerOptionButton(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = true
        
        mCellBannerOption?.mIconImageView.image = UIImage(named: "ic_check")
        
        mHiddenViewDelegate?.OnUnSelect(mClickFrom: "Banner")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    //MARK:- Two Custom Option Horizontal
    @objc func TwoOptionCustom1Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = true
        items[sender.tag].mAnswer[1].mSelected = false
        mCellTwoOptionCustom?.mIconOption1ImageView.image = UIImage(named: "ic_check")
        mCellTwoOptionCustom?.mIconOption2ImageView.image = UIImage(named: "ic_uncheck")
        
        AlertDialog.show(title: "Aviso", body: "EL PACIENTE NO TIENE DELIRIUM", view: mViewController!)
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func TwoOptionCustom2Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = true
        mCellTwoOptionCustom?.mIconOption1ImageView.image = UIImage(named: "ic_uncheck")
        mCellTwoOptionCustom?.mIconOption2ImageView.image = UIImage(named: "ic_check")
        AlertDialog.show(title: "Aviso", body: "POSITIVO CON DELIRIUM", view: mViewController!)
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ElevenOption1Button(_ sender : UIButton){
        AnswerDataSource.mEleven = 0
        
        items[sender.tag].mAnswer[0].mSelected = true
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ElevenOption2Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = true
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ElevenOption3Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = true
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    @objc func ElevenOption4Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = true
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ElevenOption5Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = true
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    @objc func ElevenOption6Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = true
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ElevenOption7Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = true
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ElevenOption8Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = true
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
        
    }
    
    @objc func ElevenOption9Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = true
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ElevenOption10Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = true
        items[sender.tag].mAnswer[10].mSelected = false
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_check")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_uncheck")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    @objc func ElevenOption11Button(_ sender : UIButton){
        items[sender.tag].mAnswer[0].mSelected = false
        items[sender.tag].mAnswer[1].mSelected = false
        items[sender.tag].mAnswer[2].mSelected = false
        items[sender.tag].mAnswer[3].mSelected = false
        items[sender.tag].mAnswer[4].mSelected = false
        items[sender.tag].mAnswer[5].mSelected = false
        items[sender.tag].mAnswer[6].mSelected = false
        items[sender.tag].mAnswer[7].mSelected = false
        items[sender.tag].mAnswer[8].mSelected = false
        items[sender.tag].mAnswer[9].mSelected = false
        items[sender.tag].mAnswer[10].mSelected = true
        mCellElevenOptionCell?.mIconOption1ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption2ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption3ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption4ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption5ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption6ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption7ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption8ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption9ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption10ImageView.image = UIImage(named:"ic_uncheck")
        mCellElevenOptionCell?.mIconOption11ImageView.image = UIImage(named:"ic_check")
        
        self.mHiddenViewDelegate?.refreshTablaData()
    }
    
    
    
    
    //MARK:- TAbleView Select Cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
