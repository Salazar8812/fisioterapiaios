//
//  GaleriaDataSource.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 31/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

protocol OnSelectItemDelegate : NSObjectProtocol {
    func onItemSelect(mNameImage : String)
}

class GaleriaDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    let reuseIdentifier = "ItemGaleriaCollectionViewCell" // also enter this string as the cell identifier in the storyboard
    var mListPhotos : [String] = []
    var mListPhotosCollectionView : UICollectionView!
    var mDelegate : OnSelectItemDelegate?
    
    init(mListPhotos : [String], mListPhotosCollectionView : UICollectionView, mDelegate : OnSelectItemDelegate) {
        super.init()
        self.mListPhotos = mListPhotos
        self.mListPhotosCollectionView = mListPhotosCollectionView
        self.mListPhotosCollectionView.delegate = self
        self.mDelegate = mDelegate
        self.mListPhotosCollectionView.dataSource = self
        let nibName = UINib(nibName: "ItemGaleriaCollectionViewCell", bundle:nil)
        mListPhotosCollectionView.register(nibName, forCellWithReuseIdentifier: "ItemGaleriaCollectionViewCell")

    }
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mListPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
         let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
               let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
               let size:CGFloat = (mListPhotosCollectionView.frame.size.width - space) / 2.0
               return CGSize(width: size, height: size)
    }

    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ItemGaleriaCollectionViewCell
        
        cell.mIconImageView.image = UIImage(named : mListPhotos[indexPath.item])
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        mDelegate?.onItemSelect(mNameImage: mListPhotos[indexPath.item])
    }

}
