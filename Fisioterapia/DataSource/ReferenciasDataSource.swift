//
//  ReferenciasDataSource.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 16/06/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ReferenciasDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [ReferenciaModel] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemReferenceTableViewCell"
    var mViewController:  UIViewController?
    
    init(tableView: UITableView, mViewController:  UIViewController) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        self.tableView?.rowHeight = UITableView.automaticDimension
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
    }
    
    func update(items: [ReferenciaModel]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem) as! ItemReferenceTableViewCell
        cell.mReferenceLabel.text = item.mReferencia
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}
