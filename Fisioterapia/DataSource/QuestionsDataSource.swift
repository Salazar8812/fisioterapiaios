//
//  QuestionsDataSource.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 15/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class QuestionsDataSource: NSObject, UITableViewDataSource, UITableViewDelegate, HiddenViewDelegate {

    var mSectionsArray : [SectionsQuestions] = []
    var tableView : UITableView?
    var mGenericContainer : String = "ItemGenericContainerOptionTableViewCell"
    var mViewController:  UIViewController?
    
    init(tableView: UITableView, mViewController:  UIViewController) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        self.tableView?.rowHeight = UITableView.automaticDimension
        let nib = UINib(nibName: mGenericContainer, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mGenericContainer)
        self.mViewController = mViewController
    }
    
    func update(items: [SectionsQuestions]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .TwoOptionsHorizontal &&
            self.mSectionsArray[indexPath.row].mTypeSection == .FALTA_ATENCION &&
            self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FIVE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_ONE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_TWO && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_THREE){
            return 230
        }else
        if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .TwoOptionsHorizontal && self.mSectionsArray[indexPath.row].mListSectionQuestions.count == 1 && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FIVE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_ONE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_TWO && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_THREE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FOUR &&
            self.mSectionsArray[indexPath.row].mTypeSection != .FALTA_ATENCION){
            return 185
        }else if (self.mSectionsArray[indexPath.row].mListSectionQuestions.count == 2 && self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .TwoOptionsHorizontal && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FIVE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_ONE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_TWO && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_THREE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FOUR){
            return 280
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .ThreeOptionsHorizontal && self.mSectionsArray[indexPath.row].mListSectionQuestions.count == 1){
            return 150
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .InputOptions){
            return 520
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .FourOptionsVertical && self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mAnswer.count > 4){
                return 350
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions.count > 8 && self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .TwoOptionsHorizontal && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FIVE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_ONE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_TWO && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_THREE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FOUR){
            return 1000
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions.count == 4 && self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .TwoOptionsHorizontal && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FIVE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_ONE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_TWO && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_THREE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FOUR){
            return 450
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .ListOption && self.mSectionsArray[indexPath.row].mListSectionQuestions.count > 8){
            return 550
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .ThreeOtionVertical){
            return 260
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .InputOptionFormulas){
            return 400
        }else if (self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .ResultOption){
            return 260
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions.count < 6 && self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .TwoOptionsHorizontal && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FIVE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_ONE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_TWO && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_THREE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FOUR){
            return 550
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions.count == 8 && self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .TwoOptionsHorizontal && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FIVE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_ONE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_TWO && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_THREE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FOUR){
            return 850
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions.count == 7 && self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .TwoOptionsHorizontal && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FIVE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_ONE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_TWO && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_THREE && self.mSectionsArray[indexPath.row].mTypeSection != .SSQ_FOUR){
            return 750
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .BannerOption){
            return 70
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .SixOptionsVertical){
            return 390
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .GenericResult){
            return 200
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .TwoOptionCustom){
            return 400
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .ItemElevenOptionVertical){
            return 570
        }else if(self.mSectionsArray[indexPath.row].mTypeSection == .SSQ_FIVE || self.mSectionsArray[indexPath.row].mTypeSection == .SSQ_ONE || self.mSectionsArray[indexPath.row].mTypeSection == .SSQ_TWO || self.mSectionsArray[indexPath.row].mTypeSection == .SSQ_THREE || self.mSectionsArray[indexPath.row].mTypeSection == .SSQ_FOUR){
            print("######3","Enter")
             return 110
        }else if(self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mTypeContainer == .FourOptionsVertical && self.mSectionsArray[indexPath.row].mListSectionQuestions[0].mAnswer.count == 4){
            return 320
        }else{
            return 325
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mGenericContainer) as! ItemGenericContainerOptionTableViewCell
        cell.mListQuestions = item.mListSectionQuestions
        cell.mTitleSectionLabel.text = item.mTitleSection
        cell.createTableView(mDelegate: self, mController: mViewController!)
        
        if(item.mListSectionQuestions[0].mTypeContainer == .InputOptionFormulas){
            cell.mMainSectionContainerView.shadowRadius = 0
            cell.mMainSectionContainerView.shadowOpacity = 0
            cell.shadowOffset = CGSize(width: 0,height: 0)
        }
       
        if(item.mListSectionQuestions[0].mTypeContainer == .ResultOption || item.mListSectionQuestions[0].mTypeContainer == .GenericResult ){
            cell.mMainSectionContainerView.shadowRadius = 0
            cell.mMainSectionContainerView.shadowOpacity = 0
            cell.setMargin()
            cell.mTopHeightContraint.constant = 10
            cell.shadowOffset = CGSize(width: 0,height: 0)
        }else{
            
            cell.mMainSectionContainerView.shadowRadius = 2
            cell.mMainSectionContainerView.shadowOpacity = 2
            cell.setMarginsDefault()
            cell.shadowOffset = CGSize(width: 2,height: 2)
        }
       
        if(item.mListSectionQuestions[0].mTypeContainer == .BannerOption){
            cell.mTopHeightContraint.constant = 10
            cell.layoutIfNeeded()
        }else{
            cell.mTopHeightContraint.constant = 47
            cell.layoutIfNeeded()
        }
        
        if(item.mListSectionQuestions[0].mTypeContainer == .TwoOptionCustom){
            cell.mTopHeightContraint.constant = 55
            cell.layoutIfNeeded()
        }
        
        if(item.mListSectionQuestions[0].mTypeContainer == .ThreeOptionsHorizontal){
            cell.mTopHeightContraint.constant = 10
            cell.layoutIfNeeded()
        }
        
        if(item.mListSectionQuestions[0].mTypeContainer == .TwoOptionsHCustom){
            cell.mTopHeightContraint.constant = 10
            cell.layoutIfNeeded()
        }
        
        
        
        if(!item.mShowSection){
            cell.mMainSectionContainerView.isHidden = true
        }else{
            cell.mMainSectionContainerView.isHidden = false
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func OnUnSelect(mClickFrom: String) {
        switch mClickFrom {
        case "Banner":
            for mIndexSection in 0...mSectionsArray.count - 1{
                       for mPosQuestion in 0...mSectionsArray[mIndexSection].mListSectionQuestions.count - 1{
                           for mPosAnswer in 0...mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer.count - 1{
                               if(mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer[mPosAnswer].mID == "EM" || mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer[mPosAnswer].mID == "AN" ){
                                   mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer[mPosAnswer].mSelected = false
                               }
                           }
                       }
                   }
            break
        case "Sedacion":
            for mIndexSection in 0...mSectionsArray.count - 1{
                for mPosQuestion in 0...mSectionsArray[mIndexSection].mListSectionQuestions.count - 1{
                    for mPosAnswer in 0...mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer.count - 1{
                        if(mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer[mPosAnswer].mID == "AN" || mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer[mPosAnswer].mID == "MBAN" ){
                            mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer[mPosAnswer].mSelected = false
                        }
                    }
                }
            }
            break
        case "Agitacion":
            for mIndexSection in 0...mSectionsArray.count - 1{
                for mPosQuestion in 0...mSectionsArray[mIndexSection].mListSectionQuestions.count - 1{
                    for mPosAnswer in 0...mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer.count - 1{
                        if(mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer[mPosAnswer].mID == "EM" || mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer[mPosAnswer].mID == "MBAN" ){
                            mSectionsArray[mIndexSection].mListSectionQuestions[mPosQuestion].mAnswer[mPosAnswer].mSelected = false
                        }
                    }
                }
            }
            break
        default:
            break
        }
     
        refreshTablaData()
    }
    
    func OnSelectEDAD(mEdad: String) {
        switch mEdad {
        case "Adulto":
            for index in 0...mSectionsArray.count - 1{
                switch mSectionsArray[index].mTypeSection {
                case .APERTURA_OCULAR:
                   
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mAnswer = "Espóntanea"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mAnswer = "Estímulo verbal"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mAnswer = "Al Dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mAnswer = "Ninguna"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mSelected = false
                    break
                    
                case .RESPUESTA_VERBAL:
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mAnswer = "Orientada"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mAnswer = "Confusa"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mAnswer = "Palabras inapropiadas"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mAnswer = "Sonidos ininteligibles"
                    
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mAnswer = "Ninguna"
                    
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mSelected = false

                    break
                    
                case .RESPUESTA_MOTORA:
                    
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mAnswer = "Obedece"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mAnswer = "Localiza el dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mAnswer = "Se retira el dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mAnswer = "Flexión anormal"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mAnswer = "Respuesta extensora"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[5].mAnswer = "Ninguna"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[5].mSelected = false

                    break
                default:
                    break
                }
            
            }
            break
        case "Niño":
            for index in 0...mSectionsArray.count - 1{
                switch mSectionsArray[index].mTypeSection {
                case .APERTURA_OCULAR:
                   
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mAnswer = "Espóntanea"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mAnswer = "Estímulo verbal"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mAnswer = "Al Dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mAnswer = "Ninguna"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mSelected = false
                    break
                    
                case .RESPUESTA_VERBAL:
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mAnswer = "Orientada, apropiada"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mAnswer = "Confusa"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mAnswer = "Palabras inapropiadas"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mAnswer = "Sonidos ininteligibles"
                    
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mAnswer = "Ninguna"
                    
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mSelected = false
                    break
                    
                case .RESPUESTA_MOTORA:
                    
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mAnswer = "Obedece ordenes"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mAnswer = "Localiza el dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mAnswer = "Se retira el dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mAnswer = "Flexión al dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mAnswer = "Respuesta al dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[5].mAnswer = "Ninguna"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[5].mSelected = false
                    break
                default:
                    break
                }
            
            }
            break
        case "Lactante":
            for index in 0...mSectionsArray.count - 1{
                switch mSectionsArray[index].mTypeSection {
                case .APERTURA_OCULAR:
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mAnswer = "Espóntanea"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mAnswer = "Estímulo verbal"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mAnswer = "Al Dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mAnswer = "Ninguna"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false

                    break
                    
                case .RESPUESTA_VERBAL:
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mAnswer = "Murmullo y balbuceo"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mAnswer = "Irritable, llora"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mAnswer = "Llora en respuesta al dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mAnswer = "Gime en respuesta al dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mAnswer = "Ninguna"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mSelected = false
                    break
                    
                case .RESPUESTA_MOTORA:
                    
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mAnswer = "Se mueve espontaneamente"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mAnswer = "Se retira en respuesta al tacto"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mAnswer = "Se retira el dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mAnswer = "Postura de decorticación al dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mAnswer = "Postura de descerebración al dolor"
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[5].mAnswer = "Ninguna"
                    
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[0].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[1].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[2].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[3].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[4].mSelected = false
                    mSectionsArray[index].mListSectionQuestions[0].mAnswer[5].mSelected = false
                    break
                default:
                    break
                }
            }
            break
        default:
            break
        }
        
        refreshTablaData()

        
    }
    
    func hideShowSection(mTypeSection: SectionQuestion, mShowSection : Bool) {
        for index in 0...mSectionsArray.count - 1{
            if(mSectionsArray[index].mTypeSection == mTypeSection){
                mSectionsArray[index].mShowSection = mShowSection
            }
        }
        self.tableView?.reloadData()
    }
    
    func refreshTablaData() {
        self.tableView?.reloadData()
    }
}
