//
//  ItemReferenceTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 16/06/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemReferenceTableViewCell: UITableViewCell {
    @IBOutlet weak var mReferenceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
