//
//  ItemFormulasTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 22/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemFormulasTableViewCell: UITableViewCell {

    @IBOutlet weak var mInputOption2TextField: UITextField!
    @IBOutlet weak var mInputOption2Label: UILabel!
    @IBOutlet weak var mInputOption1TextField: UITextField!
    @IBOutlet weak var mInputOption1Label: UILabel!
    @IBOutlet weak var mCalculateButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
