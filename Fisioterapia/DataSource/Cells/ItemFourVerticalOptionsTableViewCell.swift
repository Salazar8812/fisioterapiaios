//
//  ItemFourVerticalOptionsTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 15/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemFourVerticalOptionsTableViewCell: UITableViewCell {
   
    @IBOutlet weak var mAnswer5Label: UILabel!
    @IBOutlet weak var mOption5Button: UIButton!
    @IBOutlet weak var mIconOption5ImageView: UIImageView!
    @IBOutlet weak var mContainerOption5View: UIView!
    @IBOutlet weak var mContainerOption4View: UIView!
    @IBOutlet weak var mOption4Button: UIButton!
    @IBOutlet weak var mOption3Button: UIButton!
    @IBOutlet weak var mOption2Button: UIButton!
    @IBOutlet weak var mOption1Button: UIButton!
    @IBOutlet weak var mIconOption1ImageView: UIImageView!
    
    @IBOutlet weak var mIconOption4ImageView: UIImageView!
    @IBOutlet weak var mIconOption2ImageView: UIImageView!
    @IBOutlet weak var mIconOption3ImageView: UIImageView!
    @IBOutlet weak var mAnswer4Label: UILabel!
    @IBOutlet weak var mAnswer3Label: UILabel!
    @IBOutlet weak var mAnswer2Label: UILabel!
    @IBOutlet weak var mAnswer1Label: UILabel!
    @IBOutlet weak var mQuestionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
