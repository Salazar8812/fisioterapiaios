//
//  ItemInputOptionTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 15/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemInputOptionTableViewCell: UITableViewCell {
    @IBOutlet weak var mQuestionLabel: UILabel!
    @IBOutlet weak var mAnswerTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
