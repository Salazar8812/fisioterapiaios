//
//  ItemThreeOptionsHorizontalTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 18/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemThreeOptionsHorizontalTableViewCell: UITableViewCell {

    @IBOutlet weak var mQuestionLabel: UILabel!
    @IBOutlet weak var mAnswer1Label: UILabel!
    @IBOutlet weak var mIconOption1ImageView: UIImageView!
    @IBOutlet weak var mOption1Button: UIButton!
    
    @IBOutlet weak var mAnswer2Label: UILabel!
    
    @IBOutlet weak var mOption2Button: UIButton!
    @IBOutlet weak var mIconOption2ImgaView: UIImageView!
    @IBOutlet weak var mIconOption3ImageView: UIImageView!
    @IBOutlet weak var mAnswer3Label: UILabel!
    @IBOutlet weak var mOption3Button: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
