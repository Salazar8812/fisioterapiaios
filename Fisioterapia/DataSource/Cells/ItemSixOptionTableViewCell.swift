//
//  ItemSixOptionTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 26/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemSixOptionTableViewCell: UITableViewCell {
    @IBOutlet weak var mIconOption1ImageView: UIImageView!
    @IBOutlet weak var mOption1Button: UIButton!
    @IBOutlet weak var mAnswerOption1Label: UILabel!
    
    @IBOutlet weak var mIconOption2ImageView: UIImageView!
    @IBOutlet weak var mOption2Button: UIButton!
    @IBOutlet weak var mAnswerOption2Label: UILabel!
    
    @IBOutlet weak var mIconOption3ImageView: UIImageView!
    @IBOutlet weak var mOption3Button: UIButton!
    @IBOutlet weak var mAnswerOption3Label: UILabel!
    
    @IBOutlet weak var mIconOption4ImageView: UIImageView!
    @IBOutlet weak var mOption4Button: UIButton!
    @IBOutlet weak var mAnswerOption4Label: UILabel!
    
    @IBOutlet weak var mIconOption5ImageView: UIImageView!
    @IBOutlet weak var mOption5Button: UIButton!
    @IBOutlet weak var mAnswerOption5Label: UILabel!
    
    @IBOutlet weak var mIconOption6ImageView: UIImageView!
    @IBOutlet weak var mOption6Button: UIButton!
    @IBOutlet weak var mAnswerOption6Label: UILabel!
    
    @IBOutlet weak var mQuestionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
