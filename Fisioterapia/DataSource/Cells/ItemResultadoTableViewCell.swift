//
//  ItemResultadoTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 25/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemResultadoTableViewCell: UITableViewCell {
    @IBOutlet weak var mAnswer1: UILabel!
    @IBOutlet weak var mAnswer2: UILabel!
    @IBOutlet weak var mAnswer3: UILabel!
    @IBOutlet weak var mAnswer4: UILabel!
    @IBOutlet weak var mAnswer5: UILabel!
    @IBOutlet weak var mContentResultView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
