//
//  ItemElevenOptionTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 31/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemElevenOptionTableViewCell: UITableViewCell {
    @IBOutlet weak var mIconOption1ImageView: UIImageView!
    @IBOutlet weak var mIconOption2ImageView: UIImageView!
    @IBOutlet weak var mIconOption3ImageView: UIImageView!
    @IBOutlet weak var mIconOption4ImageView: UIImageView!
    @IBOutlet weak var mIconOption6ImageView: UIImageView!
    @IBOutlet weak var mIconOption7ImageView: UIImageView!
    @IBOutlet weak var mIconOption8ImageView: UIImageView!
    @IBOutlet weak var mIconOption9ImageView: UIImageView!
    @IBOutlet weak var mIconOption10ImageView: UIImageView!
    @IBOutlet weak var mIconOption11ImageView: UIImageView!
    @IBOutlet weak var mIconOption5ImageView: UIImageView!
    
    @IBOutlet weak var mOption1Button: UIButton!
    @IBOutlet weak var mOption2Button: UIButton!
    @IBOutlet weak var mOption3Button: UIButton!
    @IBOutlet weak var mOption4Button: UIButton!
    @IBOutlet weak var mOption5Button: UIButton!
    @IBOutlet weak var mOption6Button: UIButton!
    @IBOutlet weak var mOption7Button: UIButton!
    @IBOutlet weak var mOption8Button: UIButton!
    @IBOutlet weak var mOption9Button: UIButton!
    @IBOutlet weak var mOption10Button: UIButton!
    @IBOutlet weak var mOption11Button: UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
