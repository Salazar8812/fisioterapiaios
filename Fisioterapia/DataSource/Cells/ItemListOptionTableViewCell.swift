//
//  ItemListOptionTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 15/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemListOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var mOptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
