//
//  ItemGenericContainerOptionTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 15/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemGenericContainerOptionTableViewCell: UITableViewCell {
    @IBOutlet weak var mContainerOptionTableView: UITableView!
    var mAnswerDataSource : AnswerDataSource!
    var mListQuestions : [Question] = []
    @IBOutlet weak var mTitleSectionLabel: UILabel!
    @IBOutlet weak var mMainSectionContainerView: UIView!
    @IBOutlet weak var mTopHeightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var mLeadingContraint: NSLayoutConstraint!
    
    @IBOutlet weak var mBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var mTopConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func createTableView(mDelegate : HiddenViewDelegate, mController: UIViewController){
        mAnswerDataSource = AnswerDataSource(tableView: self.mContainerOptionTableView, mHiddenViewDelegate: mDelegate, mViewController: mController)
        mContainerOptionTableView.dataSource = mAnswerDataSource
        mContainerOptionTableView.delegate = mAnswerDataSource
        mContainerOptionTableView.reloadData()
        mAnswerDataSource.update(items: mListQuestions)
    }
    
    func setMargin(){
        mBottomConstraint.constant = 0
        mTopConstraint.constant = 0
        mLeadingContraint.constant = 0
        mTrailingConstraint.constant = 0
    }
    
    func setMarginsDefault(){
        mBottomConstraint.constant = 12
        mTopConstraint.constant = 12
        mLeadingContraint.constant = 12
        mTrailingConstraint.constant = 12
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
