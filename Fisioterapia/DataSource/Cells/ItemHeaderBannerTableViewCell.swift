//
//  ItemHeaderBannerTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 26/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemHeaderBannerTableViewCell: UITableViewCell {
    @IBOutlet weak var mBannerButton: UIButton!
    @IBOutlet weak var mIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
