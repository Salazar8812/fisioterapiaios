//
//  ItemThreeVerticalOptionsTableViewCell.swift
//  Fisioterapia
//
//  Created by Charls Salazar on 22/05/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemThreeVerticalOptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var mAnswer3Label: UILabel!
    @IBOutlet weak var mOptionButton3: UIButton!
    @IBOutlet weak var mIconOption3ImageView: UIImageView!
    @IBOutlet weak var mAnswer1Label: UILabel!
    @IBOutlet weak var mAnswer2Label: UILabel!
    @IBOutlet weak var mOptionButton2: UIButton!
    @IBOutlet weak var mIconOption2ImageView: UIImageView!
    @IBOutlet weak var mIconOption1ImageView: UIImageView!
    @IBOutlet weak var mOptionButton1: UIButton!
    @IBOutlet weak var mQuestionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
